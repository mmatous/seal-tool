# How to contribute

## General workflow

- Fork and clone the repository on GitHub
- Run `git config core.hooksPath ./hooks`
- Inspect said hooks for your own sake
- Create and modify your feature branch
- User-facing changes should be documented separately in [CHANGELOG.md](./CHANGELOG.md), roughly following [keep a changelog](https://keepachangelog.com/en/1.1.0/) convention
- Writing tests and documentation for contributed code is highly encouraged
- Push changes to your repository
- Submit a pull request

## Commit message format:

- Commits MUST indicate agreement to the [Developer Certificate of Origin (DCO)](https://developercertificate.org/) using the `--signoff` or `-s` option
- Commits SHOULD follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification; some deviations are acceptable
- Commit messages SHOULD be kept <60 characters for the first line and <100 for subsequent lines
- Commit title SHOULD start with a lowercase letter while the body SHOULD consist of full sentences following appropriate structure.
