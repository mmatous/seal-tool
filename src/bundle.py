#!/usr/bin/env python3

import glob
import platform
import subprocess
from pathlib import Path

if platform.system() == 'Windows':
    conf_path = './venv/lib/site-packages/alive_progress/core/configuration.py'
else:
    conf_path = './venv/lib/python*/site-packages/alive_progress/core/configuration.py'

aps = glob.glob(conf_path)  # noqa: PTH207
if not len(aps) == 1:
    raise RuntimeError('alive_progress/core/configuration.py')
ap = Path(aps[0])
ap_text = ap.read_text().splitlines()
try:
    del_idx, _ = next(line for line in enumerate(ap_text) if ' == func_file' in line[1])
except StopIteration:
    print('alive-progress is likely fixed in this environment')
else:
    del ap_text[del_idx]
    mod_idx = del_idx - 1
    if ap_text[mod_idx][-1] != '\\':
        raise RuntimeError
    ap_text[mod_idx] = ap_text[mod_idx].replace(' \\', ':')
    ap.write_text('\n'.join(ap_text))

sys_type = f'{platform.system()}-{platform.machine()}'
head = Path('./.git/HEAD').read_text().strip()
# head is hash or head is ref, hash is in ref, skip 'ref: '
sha = head if '/' not in head else (Path('./.git') / head[5:]).read_text().strip()
bundler_cmd = [
    'pyinstaller',
    './src/seal/__main__.py',
    '--noconfirm',
    '--collect-all',
    'grapheme',
    '--hidden-import',
    'matplotlib.backends.backend_svg',
    '--onefile',
    '--name',
    f'seal-{sys_type}-{sha[:10]}'.lower(),
]
subprocess.run(bundler_cmd, check=True)  # noqa: S603
