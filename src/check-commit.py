#!/usr/bin/env python3

import os
import re
import sys

import git

curr_branch = os.environ['CI_COMMIT_BRANCH']
def_branch = os.environ['CI_REPO_DEFAULT_BRANCH']
print(f'Linting commits from {def_branch} HEAD up to {curr_branch}')
repo = git.Repo('./')
repo.remotes.origin.fetch(f'refs/heads/{def_branch}:refs/heads/origin')
commits = repo.iter_commits(f'origin/{def_branch}..{curr_branch}', reverse=True)
for commit in commits:
    commit_msg = str(commit.message)
    committer_mail = commit.author.email
    commit_lines = commit_msg.splitlines()
    print(f'Processing {commit.hexsha[:10]} {commit_msg.splitlines()[0]}')

    # ignore possible quotes and specials e.g. <, , >, they are
    # discouraged by https://www.rfc-editor.org/rfc/rfc5321#section-4.1.2 anyway
    sgn_re = re.compile(r'Signed-off-by: .+ <(?P<mail>\S+@\S+\.\S+)>')
    signoff_mails = (sgn_re.fullmatch(line) for line in commit_lines[2:])
    signoff_mails = (m.group('mail') for m in signoff_mails if m)
    if committer_mail not in signoff_mails:
        print(
            'Commit not signed off by the author. Please run\n'
            'git rebase --signoff HEAD~N\n'
            'with N being the number of commits you need to sign off on.'
        )
        sys.exit(1)

    if commit_lines[0].startswith("Merge branch '"):
        print('Merge commit. Skipping the rest of checks.')
        continue

    # others are permissible, but rare; will add as necessary
    prefixes = ['build', 'chore', 'ci', 'docs', 'fix', 'feat', 'perf', 'refactor', 'revert', 'style', 'test']
    prefix_group = f'({'|'.join(prefixes)})'
    scope_group_opt = r'(\(\w+\))?'
    break_opt = r'(?P<firstline_breaking>!)?'
    cc_str = rf'{prefix_group}{scope_group_opt}{break_opt}: \S+.*'
    first_fm = re.fullmatch(cc_str, commit_lines[0])
    if not first_fm:
        print(
            'The first line of commit message does not seem to follow conventional commits specification.\n'
            'See https://www.conventionalcommits.org/ for more information.'
        )
        sys.exit(2)
    if not commit_lines[2][0].isupper():
        print('Commit message body (or sign off) should start with a capital letter.')
        sys.exit(3)

    firstline_breaking = first_fm.group('firstline_breaking')
    break_explained = any(line.startswith('BREAKING CHANGE: ') for line in commit_lines)
    if firstline_breaking and not break_explained:
        print(
            'Breaking change detected based on !, but no additional explanation given.\n'
            'Please explain why are committed changes breaking and possible course of action in "BREAKING CHANGE: " footer.'
        )
        sys.exit(4)
    elif break_explained and not firstline_breaking:
        print(
            'Breaking change detected based on "BREAKING CHANGE: " footer, but first line'
            ' not denoted as breaking using ! marker.'
        )
        sys.exit(5)
