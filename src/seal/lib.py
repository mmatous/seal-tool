"""Helpers and thin wrappers for core functionality, meant to be used in seal lib."""

import logging
from collections.abc import Sequence
from pathlib import Path

from seal.analyze import analyze_grid, save_results
from seal.config import Analysis, Analysis0, Analysis2, Analysis3, Config, normalize_param
from seal.types import AnalysisResults
from seal.util import make_grid

logger = logging.getLogger(__name__)


def empty(_a: str, _b: int) -> None:
    """Empty hook."""


def analyze(cfg: Config, prefix: str = 'seal-') -> AnalysisResults:
    """Perform requested analyses on specified encounters.

    :param cfg: configuration with `encounters` and (optionally) `quadrat_list` set to DataFrame
        `encounters` must contain at least coord_x and coord_y integer columns and species string column
        `quadrat_list` must contain coord_x and coord_y integer MultiIndex
    :param prefix: string prepended to saved analysis results, ignored if `cfg.out_dir` is `None`
    :return: dictionary of analysis names and results (dataframe, string, None) for requested analyses
    :raises AttributeError: if mandatory column `species` is missing
    :raises KeyError: if any mandatory columns `coord_x`, `coord_y` is missing
    :raises UnsuitableGridError: if loaded grid is not suitable for requested levels or analyses
    """
    grid = make_grid(cfg)
    results = analyze_grid(grid, cfg, {'pre': empty, 'post': empty})
    return handle_results(cfg.out_dir, prefix, results)


def handle_results(out_dir: Path | None, prefix: str, results: AnalysisResults) -> AnalysisResults:
    if out_dir is not None:
        logger.info(f'saving results to {out_dir}')
        for info, result in results.items():
            save_results(prefix, out_dir, info, result)
    return results


def make_analyses(analyses: Sequence[dict[str, str | int]]) -> list[Analysis]:
    """Return list of analysis classes as expected by `Config.new()`.

    :param analyses: a sequence of dicts matching {'type': 'analysis_type', 'optional_key': 'analysis_specific_setting'}

    >>> make_analyses([{'type': 'a1'}, {'type': 'a2', 'permutations': 50}])
    [Analysis0(type_='a1'), Analysis2(type_='a2', permutations=50)]
    """
    res: list[Analysis0 | Analysis2 | Analysis3] = []
    analyses = normalize_param(analyses)
    for a in analyses:
        if a['type_'] == 'a2':
            res.append(Analysis2(**a))  # type: ignore[arg-type]
        elif a['type_'] == 'a3':
            res.append(Analysis3(**a))  # type: ignore[arg-type]
        else:
            res.append(Analysis0(**a))  # type: ignore[arg-type]
    return res
