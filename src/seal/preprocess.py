import argparse
from typing import Any, Final, cast

import pandas as pd

from seal.types import PreprocessCheck
from seal.util import enc_dtypes

DEFAULT_CHECKS: Final[list[PreprocessCheck]] = ['nas', 'strs']


def main(args: argparse.Namespace) -> int:
    enc = pd.read_csv(args.dataset, dtype=enc_dtypes(), parse_dates=['date']).rename(columns=str.lower).convert_dtypes()
    pd.set_option('display.max_seq_items', None)

    errs = check_encounters(enc, args.checks)
    if 'nas' in errs and not errs['nas'].empty:
        enc = handle_nas(enc, cast('pd.Index[int]', errs['nas']), args)
    if 'strs' in errs and not errs['strs'].empty:
        df = cast(pd.DataFrame, errs['strs'])  # the only key with df value
        bad_only = df.dropna(how='all')
        if not bad_only.empty:
            enc = handle_bad_strs(enc, bad_only, df)
    if 'dups' in errs and not errs['dups'].empty:
        print(f'The following rows may be duplicates:\n{errs['dups']}\n')
    if 'refs' in errs and not errs['refs'].empty:
        print(f'The following values of "ref" column are out of place:\n{errs['refs']}.\n')
    if 'species-name' in errs and not errs['species-name'].empty:
        print(f'The following species or name occurs in more than one pairing:\n{errs['species-name']}\n')
    if 'species-phase-morph' in errs and not errs['species-phase-morph'].empty:
        handle_bad_combinations(enc, cast('pd.Index[int]', errs['species-phase-morph']))
    if 'morph-species' in errs and not errs['morph-species'].empty:
        print(f'The following morphs do not match expected species:\n{errs['morph-species']}\n')
    if 'family' in errs and not errs['family'].empty:
        print('The following families have unexpected phases:')
        enc.loc[cast('pd.Index[int]', errs['family']), 'phase'] = 'term'
        print(f'Converted these "ad" phases to "term"\n{errs['family']}\n')
    if 'individuals' in errs and not errs['individuals'].empty:
        print(f'The following rows contain invalid number of individuals:\n{errs['individuals']}\n')
    if enc.empty:
        print('Result is empty. No data will be written.')

    args.output.parent.mkdir(parents=True, exist_ok=True)
    enc.to_csv(args.output, index=False)
    print(f'Done. {enc.shape[0]} rows written to {args.output}.')
    return 0


def check_encounters(
    enc: pd.DataFrame, checks: list[PreprocessCheck] = DEFAULT_CHECKS
) -> dict[PreprocessCheck, 'pd.Index[int] | pd.DataFrame']:
    """Check encounters data for possible errors.

    :param enc: encounters data to check
    :param checks: list of checks to perform. Extra keywords will be ignored.
    :returns: dictionary with keys from `checks` and results of checks as corresponding values
    :raises KeyError: if check that requires missing column is requested
    """
    res: dict[PreprocessCheck, pd.Index[int] | pd.DataFrame] = {}
    # generally applicable
    if 'nas' in checks:
        res['nas'] = check_nas(enc)
    if 'strs' in checks:
        res['strs'] = check_clean_strs(enc)
    # generally applicable but requires some kind of discriminant column -> not in defaults
    if 'dups' in checks:
        res['dups'] = enc.index[enc.duplicated(keep=False)]
    # requires specific columns, aside from mandatory ones -> not in defaults
    if 'refs' in checks:  # assumes ref
        res['refs'] = check_ref(enc)
    if 'species-name' in checks:  # assumes name
        res['species-name'] = check_species_name(enc)
    if 'species-phase-morph' in checks:  # assumes phase, morph
        res['species-phase-morph'] = check_species_phase_morph(enc)
    if 'morph-species' in checks:  # assumes morph
        res['morph-species'] = check_morph_species(enc)
    if 'family' in checks:  # assumes family
        res['family'] = check_family(enc)
    if 'individuals' in checks:  # assumes individuals
        res['individuals'] = enc.index[enc['individuals'] < 1]
    return res


def check_nas(enc: pd.DataFrame) -> 'pd.Index[int]':
    """Return indices in `enc` where values are NA.

    >>> enc = pd.DataFrame(
    ...     {
    ...         'locality': [0, pd.NA, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
    ...         'transect': [0, 1, pd.NA, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
    ...         'coord_x': [0, 1, pd.NA, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
    ...         'coord_y': [0, 1, 2, pd.NA, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
    ...         't': [0, 1, 2, 3, pd.NA, 5, 6, 7, 8, 9, 10, 11, 12, 13],
    ...         'species': [0, 1, 2, 3, 4, pd.NA, 6, 7, 8, 9, 10, 11, 12, 13],
    ...         'name': [0, 1, 2, 3, 4, 5, pd.NA, 7, 8, 9, 10, 11, 12, 13],
    ...         'morph': [0, 1, 2, 3, 4, 5, 6, pd.NA, 8, 9, 10, 11, 12, 13],
    ...         'individuals': [0, 1, 2, 3, 4, 5, 6, 7, pd.NA, 9, 10, 11, 12, 13],
    ...         'significant_size': [0, 1, 2, 3, 4, 5, 6, 7, 8, pd.NA, 10, 11, 12, 13],
    ...         'family': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, pd.NA, 11, 12, 13],
    ...         'phase': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, pd.NA, 12, 13],
    ...         'quadrat': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, pd.NA, 13],
    ...         'quadrat_id': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, pd.NA],
    ...     }
    ... ).convert_dtypes()
    >>> check_nas(enc)
    Index([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], dtype='int64')
    """
    col_has_nas = enc.isna().any(axis='columns')
    return enc[col_has_nas].index


def handle_nas(enc: pd.DataFrame, na_indices: 'pd.Index[int]', args: argparse.Namespace) -> pd.DataFrame:
    nas = enc.loc[na_indices]
    info = ['The following rows are missing important values:']
    for idx, row in nas.iterrows():
        nas_columns = ', '.join(row[row.isna()].index.to_list())
        info.append(f'Row {idx} NA columns: {nas_columns}')
    info.append('\n')
    nas_info = '\n'.join(info)
    print(nas_info)

    if args.drop_nas:
        enc = enc[~enc.index.isin(na_indices)]
    elif args.drop_nas is None:
        drop = input('[D]rop/[I]gnore?\n').lower()
        if 'drop'.startswith(drop):
            enc = enc[~enc.index.isin(na_indices)]
    return enc


def check_ref(enc: pd.DataFrame) -> 'pd.Index[Any]':
    """Return indices of `enc` where they differ from the expected 1-inf well behaved series.

    >>> enc = pd.DataFrame({'ref': [1, 2, 3, 4, 5, 6]}).convert_dtypes()
    >>> check_ref(enc)
    Index([], dtype='int64')

    >>> enc = pd.DataFrame({'ref': [1, pd.NA, 3, 5, 4, 6]}).convert_dtypes()
    >>> check_ref(enc)
    Index([1, 3, 4], dtype='int64')

    >>> enc = pd.DataFrame({'ref': [1, 2, 4, 5, 6]}).convert_dtypes()
    >>> check_ref(enc)
    Index([2, 3, 4], dtype='int64')
    """
    good_ref = pd.Series(range(1, enc.shape[0] + 1), dtype=enc.ref.dtype)
    diffs = enc[(enc.ref != good_ref) | enc.ref.isna()].index
    return diffs


def check_clean_strs(enc: pd.DataFrame) -> pd.DataFrame:
    r"""Return new DataFrame containing values where any whitespace was NOT a simple space in the original DataFrame with the rest of values being NA.

    >>> enc = pd.DataFrame(
    ...     {
    ...         'locality': ['good1', 'go od 2', 'ba  d', 'ba\u00a0d', 'good3'],
    ...         'species': ['b\tad', 'good4', 'good5', 'bad  ', 'good7'],
    ...     }
    ... ).convert_dtypes()
    >>> check_clean_strs(enc)
      locality species
    0     <NA>    b ad
    1     <NA>    <NA>
    2     ba d    <NA>
    3     ba d     bad
    4     <NA>    <NA>
    """
    strs = enc.select_dtypes(['string'])
    cleaned_strs = strs.apply(lambda col: col.str.strip().replace(r'\s+', ' ', regex=True))
    diff = cast(pd.DataFrame, cleaned_strs[strs != cleaned_strs])
    return diff


def handle_bad_strs(enc: pd.DataFrame, bad_only: pd.DataFrame, cleaned: pd.DataFrame) -> pd.DataFrame:
    print('The following strings were cleaned:')
    for idx, row in bad_only.iterrows():
        fixed_columns = ', '.join(row[~row.isna()].index.to_list())
        print(f'Fixed string(s) in row {idx}, columns: {fixed_columns}')
    print('\n')

    # get inverse of bad only
    # combine inverse and enc

    for name in enc.columns:
        if name not in cleaned.columns:
            cleaned[name] = pd.Series([pd.NA] * enc.shape[0])
    cleaned = cleaned[enc.columns]
    enc = enc.where(cleaned.isna() | (enc.equals(cleaned)), cleaned)
    return enc


def check_species_name(enc: pd.DataFrame) -> 'pd.Index[Any]':
    """Return indices in `enc` where single species has multiple values in name or vice versa.

    This should not happen as name is simply a translation of species.

    >>> enc = pd.DataFrame(
    ...     data={'species': ['spp1', 'spp2', 'spp2', 'spp3', 'spp4'], 'name': ['a', 'c', 'b', 'c', 'b']}
    ... ).convert_dtypes()
    >>> check_species_name(enc)
    Index([1, 2, 3, 4], dtype='int64')
    """
    spp_name = enc[['species', 'name']].drop_duplicates()
    spp_dups = spp_name.duplicated(subset='species', keep=False)
    name_dups = spp_name.duplicated(subset='name', keep=False)
    return spp_name[spp_dups | name_dups].index


def check_species_phase_morph(enc: pd.DataFrame) -> 'pd.Index[Any]':
    """Return indices in `enc` where a species in given phase occurs with more than one morph.

    This should not happen as species in given life phase are assigned only to one morphotaxon.

    >>> enc = pd.DataFrame(
    ...     data={
    ...         'species': ['spp1', 'spp2', 'spp2', 'spp1', 'spp1'],
    ...         'phase': ['1', '1', '2', '1', '1'],
    ...         'morph': ['a', 'a', 'b', 'a', 'b'],
    ...     }
    ... ).convert_dtypes()
    >>> check_species_phase_morph(enc)
    Index([0, 4], dtype='int64')
    """
    trio = enc[['species', 'phase', 'morph']].drop_duplicates()
    dups_bools = trio.duplicated(subset=['species', 'phase'], keep=False)
    return trio[dups_bools].index


def handle_bad_combinations(enc: pd.DataFrame, diff: 'pd.Index[int]') -> None:
    bad = enc.loc[diff, ['species', 'phase', 'name', 'morph']]
    bad_groups = bad.groupby(['species', 'phase'], observed=True)
    print(f'The following ((species, phase), morph) pairs occur in multiple combinations:\n{diff}')
    for k, v in bad_groups:
        print(f'\n{k} paired-up simultaneously with\n{v.T}')
    print('\n')


def check_morph_species(enc: pd.DataFrame) -> 'pd.Index[Any]':
    """Return indices in `enc` with invalid morph values.

    Morph is considered invalid where the it is either not identical with the species
    or does not start with 'M ', which indicates a morphotaxon.

    This should not happen as either the species itself should be known or the morphotaxon should be specified.

    >>> enc = pd.DataFrame(
    ...     data={
    ...         'morph': ['M morph0', 'M morph1', 'm morph0', 'Morph2', 'morph3'],
    ...         'species': ['spp0', 'spp1', 'm morph0', 'spp2', 'spp3'],
    ...     }
    ... ).convert_dtypes()
    >>> check_morph_species(enc)
    Index([3, 4], dtype='int64')
    """
    non_m = enc[~enc.morph.str.startswith('M ')]
    return non_m[non_m.morph != non_m.species].index


def check_family(enc: pd.DataFrame) -> 'pd.Index[Any]':
    """Return indices in `enc` where species is "Labridae" or "Scaridae" yet phase is "ad".

    This should not occur as while other families have only juvenile and adult phase in our datasets,
    Labridae and Scaridae can only be in juvenile, initial or terminal phase.

    >>> enc = pd.DataFrame(
    ...     data={
    ...         'phase': ['ad', 'juv', 'init', 'term', 'ad'],
    ...         'family': ['Labridae', 'Labridae', 'Scaridae', 'Scaridae', 'Scaridae'],
    ...     }
    ... ).convert_dtypes()
    >>> check_family(enc)
    Index([0, 4], dtype='int64')
    """
    lab_scar = enc[(enc.family == 'Labridae') | (enc.family == 'Scaridae')]
    return lab_scar[lab_scar.phase == 'ad'].index
