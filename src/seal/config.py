import tomllib
from datetime import datetime
from functools import singledispatch
from keyword import iskeyword, issoftkeyword
from os import PathLike
from pathlib import Path
from typing import Any, Literal, Self

import pandas as pd
from pydantic import ConfigDict, Field, PositiveFloat, PositiveInt, model_validator
from pydantic.dataclasses import dataclass

from seal.types import Distance, ErrorStyle, ErrorType, Format, LevelStrategy, QuadratType, SamplingDirection

type AnalysisNameShort = Literal['a1', 'a4', 'a5', 'a6', 'a7', 'a8']
type AnalysisNameLong = Literal['overview', 'extotal', 'oerich', 'sratios', 'jaccard', 'abundance']


@dataclass(config=ConfigDict(extra='forbid', validate_assignment=True, validate_default=True))
class Analysis0:
    type_: AnalysisNameShort | AnalysisNameLong


@dataclass(config=ConfigDict(extra='forbid', validate_assignment=True, validate_default=True))
class Analysis2:
    type_: Literal['a2', 'sar']
    permutations: PositiveInt = 200


@dataclass(config=ConfigDict(extra='forbid', validate_assignment=True, validate_default=True))
class Analysis3:
    type_: Literal['a3', 'spdiff']
    interval: PositiveFloat | None = None


type Analysis = Analysis0 | Analysis2 | Analysis3


@dataclass(config=ConfigDict(extra='forbid', validate_assignment=True, validate_default=True))
class Sides:
    """Represents side lengths of quadrats."""

    x: PositiveFloat
    y: PositiveFloat


@dataclass(config=ConfigDict(extra='forbid', validate_assignment=True, validate_default=True))
class Plot:
    error_type: ErrorType = 'pi'
    error_style: ErrorStyle = 'band'
    logscale_x: bool = True
    output_format: Format = 'svg'


@dataclass(
    config=ConfigDict(arbitrary_types_allowed=True, extra='forbid', validate_assignment=True, validate_default=True)
)
class Config:
    """Configuration necessary for analysis and plotting.

    :raises ValidationError: if passed options do not translate into valid taskfile
    :seealso: ./tasks/example-task.toml for options explanation
    """

    encounters: Path | pd.DataFrame
    quadrat_sides: Sides

    out_dir: Path | None = None
    levels: list[PositiveInt] = Field(default=[1, 2, 3])
    level_strategy: LevelStrategy = 'nested-quadrats'
    direction: SamplingDirection = 'any'
    include_tiny: bool = True
    quadrat_types: list[QuadratType] | None = None
    distance_type: Distance = 'euclid-diagonal'
    discard_zone_info: bool = False
    discard_transect_info: bool = False
    discard_indistinguishable: bool = True
    exclude_families: list[str] = Field(default_factory=list)
    include_families: list[str] = Field(default_factory=list)
    exclude_phases: list[str] = Field(default_factory=list)
    seed: int | None = None
    use_morph: bool = False
    locality_name: str | None = None
    quadrat_list: Path | pd.DataFrame | None = None
    to: datetime | None = None
    from_: datetime | None = None

    analyses: list[Analysis] = Field(default_factory=list)
    plot: Plot = Field(default_factory=Plot)

    @classmethod
    def from_dict(cls, cfg_dict: dict[str, Any]) -> Self:
        """Return Config with the provided parameters with remaining set to default.

        :param encounters: path to encounters dataset in CSV format
        :param sides: length of quadrat's sides, any unit of length
        :raises ValidationError: if passed options do not translate into valid taskfile
        :seealso: ./tasks/example-task.toml for options explanation
        """
        return cls(**normalize_param(cfg_dict))

    @classmethod
    def from_taskfile(cls, taskfile: PathLike[str] | str) -> Self:
        """Return Config initialized with settings from a taskfile.

        :param taskfile: path to TOML taskfile
        :raises OSError: if file could not be read
        :raises ValidationError: if file is not a valid taskfile
        :seealso: ./tasks/example-task.toml for documented example
        """
        with Path(taskfile).open('rb') as task:
            task_dict = tomllib.load(task)
        return cls.from_dict(task_dict)

    @classmethod
    def new(cls, encounters: PathLike[str] | str, sides: tuple[float, float], **kwargs: Any) -> Self:  # noqa: ANN401
        """Return Config with the provided parameters with remaining set to default.

        :param encounters: path to encounters dataset in CSV format
        :param sides: length of quadrat's sides, any unit of length
        :param kwargs: any other optional arguments, same as taskfile key/value pairs
        :raises ValidationError: if passed options do not translate into valid taskfile

        `encounters` and `sides` are mandatory arguments to construct a working Config.
        Additional Config members may be specified in `kwargs`.
        """
        if 'sides' in kwargs:
            s = kwargs['sides']
            del kwargs['sides']
        else:
            s = Sides(*sides)
        return cls(encounters=Path(encounters), quadrat_sides=s, **kwargs)

    @model_validator(mode='after')
    def _merge_qs_for_transect_strats(self) -> Self:
        if self.level_strategy in ('repeated-transect-merging', 'striped-transect-merging'):
            self.__dict__['discard_zone_info'] = True  # workaround for https://github.com/pydantic/pydantic/issues/6597
        return self


@singledispatch
def normalize_param[T](params: T) -> T:
    return params


@normalize_param.register(dict)  # type spec necessary, can't handle subtypes otherwise
def _(params: dict[str, Any]) -> dict[str, Any]:  # type: ignore[misc]
    """Replace all kebab-case keys with snake-case and append an underscore to reserved keywords.

    >>> normalize_param({'a-a': 'a-a', 'b-b': ['c-c', {'d-d': {'e': 'f', 'g-g': 'h'}}]})
    {'a_a': 'a-a', 'b_b': ['c-c', {'d_d': {'e': 'f', 'g_g': 'h'}}]}
    """
    normalized = {}
    for k, v in params.items():
        norm_v = normalize_param(v)
        norm_k = k.replace('-', '_')
        norm_k = f'{norm_k}_' if iskw(norm_k) else norm_k
        normalized[norm_k] = norm_v
    return normalized


@normalize_param.register(list)
def _(params: list[Any]) -> list[Any]:  # type: ignore[misc]
    """Replace all kebab-case elements with snake-case and append an underscore to reserved keywords.

    >>> normalize_param(['a-a', {'b-b': ['c-c', {'d-d': {'e': 'f', 'g-g': 'h'}}]}])
    ['a-a', {'b_b': ['c-c', {'d_d': {'e': 'f', 'g_g': 'h'}}]}]
    """
    return [normalize_param(v) for v in params]


def iskw(s: str) -> bool:
    """Return True if `s` is a Python keyword.

    >>> iskw('from')
    True
    >>> iskw('type')
    True
    >>> iskw('species')
    False
    """
    return iskeyword(s) or issoftkeyword(s)
