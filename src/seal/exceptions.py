from collections.abc import Iterable

from pandas import DataFrame

from seal.types import Axis, Coord, InputType


class InvalidQuadratsError(Exception):
    """Raised when sp. encountered in quadrat not found in quadrat list."""

    def __init__(self, coords: Iterable[Coord], *, post_adj: bool = False) -> None:
        msg = (
            'Filtered dataset contains encounters in quadrats not found in quadrat list.'
            "Possible causes may be using quadrat list for unintended locality, incorrectly set-up filters, e.g. *from* and *to* dates in taskfile's locality section or simply mistakes/typos in provided quadrat list or dataset."
            'Extraneous coordinates:\n'
            f'{sorted(coords)}'
        )
        if post_adj:
            msg += 'As this happened post-coordinate adjustment, the most likely reason is bug in the program.'
        super().__init__(msg)


class MissingValuesError(Exception):
    """Raised when attribute considered mandatory is NA."""

    def __init__(self, rows: DataFrame, *, post_adj: bool = False) -> None:
        msg = (
            'Processed data contains missing values. This should not happen for properly preprocessed data.'
            'Rows with missing values:\n'
            f'{rows}'
        )
        if post_adj:
            msg += 'As this happened post-coordinate adjustment, the most likely reason is bug in the program.'
        super().__init__(msg)


class InvalidLiteralError(ValueError):
    """Raised when string argument is passed with invalid choice."""

    def __init__(self, selfdoc: str) -> None:
        msg = f'argument {selfdoc} is not a valid choice'
        super().__init__(msg)


class InvalidFilterError(ValueError):
    """Raised when filtering using missing columns is requested."""

    def __init__(self, field: str, input_type: InputType) -> None:
        self.field = field
        self.input_type = input_type
        msg = f'requested filtering on attribute "{self.field}", but input {self.input_type} contains no such column'
        super().__init__(msg)


class UnsuitableGridError(ValueError):
    """Grid dimensions are not suitable for analysis with chosen level(s) and/or strategy."""

    def __init__(self, operation: str, levels: list[int] | None, axis: Axis, add_info: str | None) -> None:
        """Init self.

        :param operation: operation that failed
        :param levels: problematic levels (if any)
        :param axis: problematic axis
        :param add_info: any additional description of problem that might be useful
        """
        self.operation = operation
        self.levels = levels
        self.axis = axis
        self.add_info = add_info

        msg = f'unsuitable {self.axis} dimension for {self.operation}'
        if self.levels:
            msg += f'\nproblematic levels: {self.levels}'
        if self.add_info:
            msg += f'\n{self.add_info}'
        super().__init__(msg)


class InvalidConfigError(ValueError):
    """Raised for invalid combination of options in Config."""

    def __init__(self, err_opts: list[tuple[str, str, str]]) -> None:
        """Init self.

        :param err_opts: list of triples of (variable_name, current_type, expected_type)
        """
        self.err_opts = err_opts
        msg = [f'{t[0]} is a {t[1]}, but {t[2]} was expected' for t in self.err_opts]
        super().__init__('\n'.join(msg))
