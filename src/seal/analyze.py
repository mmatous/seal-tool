import argparse
import copy
import logging
import random
from collections.abc import Callable
from functools import reduce
from pathlib import Path
from typing import Final, Literal, NamedTuple, cast

import numpy as np
import pandas as pd
from alive_progress import alive_bar  # type: ignore[import-untyped]

import seal.config as conf
from seal.config import Analysis, Analysis2, Analysis3, Sides
from seal.exceptions import (
    InvalidLiteralError,
    InvalidQuadratsError,
    MissingValuesError,
    UnsuitableGridError,
)
from seal.types import (
    A1Aux,
    AnalysisResult,
    AnalysisResults,
    Auxiliary,
    Coord,
    Distance,
    Encounters,
    GridInfo,
    LevelStrategy,
    QList,
    Result,
)
from seal.util import analysis_results_filename, direction_difference, geom_r2, get_spinner, make_grid, transform_grid

type AnalysisPhase = Literal['pre', 'post']
type DistFunction = Callable[[pd.DataFrame, pd.DataFrame], 'pd.Series[float]']
type Hooks = dict[AnalysisPhase, Callable[[str, int], None]]
type SppMatrix = pd.DataFrame


class LevelInfo(NamedTuple):
    """Contains level- (and strategy-) dependent information about the study grid."""

    grid: GridInfo
    dmat: pd.DataFrame
    sides: Sides


logger = logging.getLogger(__name__)


def main(args: argparse.Namespace) -> int:
    """Load taskfile configuration, datasets, perform and save requested analyses.

    Create output directory if necessary.
    """
    cfg = conf.Config.from_taskfile(args.taskfile)
    cfg.out_dir = Path('./results') if cfg.out_dir is None else cfg.out_dir
    spinner = get_spinner()
    n_tasks = len(set(cfg.levels)) * len(cfg.analyses)
    cfg.out_dir.mkdir(parents=True, exist_ok=True)
    with alive_bar(n_tasks, title='Analysis', spinner=spinner) as bar:

        def pre_analysis(type_: str, level: int) -> None:
            bar.text(f'{type_}, level {level}')

        def post_analysis(_0: str, _1: int) -> None:
            bar()

        results = analyze_file(cfg, {'pre': pre_analysis, 'post': post_analysis})
        for info, result in results.items():
            save_results(f'{args.taskfile.stem}-', cfg.out_dir, info, result)
    return 0


def analyze_file(cfg: conf.Config, hooks: Hooks) -> AnalysisResults:
    grid = make_grid(cfg)
    return analyze_grid(grid, cfg, hooks)


def analyze_grid(grid: GridInfo, cfg: conf.Config, hooks: Hooks) -> AnalysisResults:
    check_grid_consistency(grid)
    (grid, sides_adj) = transform_grid(grid, cfg)
    check_grid_suitability(grid.qlist, cfg.level_strategy, cfg.levels)

    lvl_infos = get_lvl_infos(grid, sides_adj, cfg)
    analysis_pos = {analysis.type_: 0 for analysis in cfg.analyses}
    results = {}
    for analysis in cfg.analyses:
        parts = []
        for level, lvl_info in lvl_infos.items():
            hooks['pre'](analysis.type_, level)
            try:
                part = analyse_level(lvl_info, analysis, cfg.seed)
            except UnsuitableGridError as e:
                x_size = lvl_info.grid.qlist.index.get_level_values('coord_x').nunique()
                e.add_note(f'is {x_size} for level {level}/{max(cfg.levels)}')
                logger.exception('Error during analysis')
                raise
            parts.append(part)
            hooks['post'](analysis.type_, level)
        a_info = (str(analysis.type_), analysis_pos[analysis.type_])
        results[a_info] = concat_results(parts, cfg.levels)
        analysis_pos[analysis.type_] += 1
    return results


def concat_results(parts: list[AnalysisResult], levels: list[int]) -> AnalysisResult:
    aux_df = parts and isinstance(parts[0][1], pd.DataFrame)
    aux_parts: list[Auxiliary] = []
    for (res, aux), lvl in zip(parts, levels, strict=True):
        res['level'] = lvl
        if aux_df:
            aux = cast(pd.DataFrame, aux)
            aux['level'] = lvl
            aux_parts.append(aux)
        elif aux is not None:
            aux = cast(A1Aux, aux)
            aux_parts.append({(lvl, k[1]): v for k, v in aux.items()})

    res_concat = pd.concat((res[0] for res in parts), ignore_index=True)
    aux_concat: Auxiliary = None
    if aux_parts:
        if aux_df:
            aux_concat = pd.concat(aux_parts, ignore_index=True)  # type: ignore[arg-type]  # every elem is df
        else:
            # every elem is dict, | is valid
            aux_concat = reduce(lambda a, b: a | b, aux_parts)  # type: ignore[operator]
            aux_concat = cast(A1Aux, aux_concat)
    return AnalysisResult(res_concat, aux_concat)


def analyse_level(
    lvl_info: LevelInfo,
    analysis: Analysis,
    seed: int | None,
) -> AnalysisResult:
    """Return results of `analysis` on passed grid with given config settings."""
    if seed:
        random.seed(seed)
        np.random.seed(seed)

    spp_matrix = get_spp_matrix(lvl_info.grid)
    res: tuple[Result, Auxiliary]
    match analysis.type_:
        case 'a1' | 'overview':
            res = a1_overview(lvl_info.grid)
        case 'a2' | 'sar':
            res = a2_sar(spp_matrix, cast(Analysis2, analysis), lvl_info.sides)
        case 'a3' | 'spdiff':
            res = a3_species_delta_sum(spp_matrix, lvl_info.dmat, cast(Analysis3, analysis))
        case 'a4' | 'extotal':
            res = a4_extent_total(spp_matrix, lvl_info.dmat)
        case 'a5' | 'oerich':
            res = a5_observed_expected(spp_matrix)
        case 'a6' | 'sratios':
            res = a6_shared_ratios(spp_matrix, lvl_info.dmat)
        case 'a7' | 'jaccard':
            res = a7_jaccard_diss(spp_matrix, lvl_info.dmat)
        case 'a8' | 'abundance':
            res = a8_abundance(lvl_info.grid)
        case _:
            raise InvalidLiteralError(f'{analysis.type_=}')
    return AnalysisResult(*res)


def a1_overview(grid: GridInfo) -> tuple[pd.DataFrame, A1Aux]:
    """Return DataFrame containing number of unique species as well as number of individuals encountered in respective quadrats.

    Auxiliary data contains per-level descriptive statistics about encounters being processed.
    """
    enc, quadrats = grid.enc, grid.qlist
    aux: A1Aux = {}
    # level is unknown so prefill 0 now, fill properly in concat_results
    aux[(0, 'desc_full')] = describe_full(enc)

    by_qid = enc.groupby(['coord_x', 'coord_y'])
    unique = by_qid.species.nunique()
    # fill 0 for quadrats where nothing was encountered
    unique = unique.reindex(quadrats.index, fill_value=0).to_frame('n_species')
    if 'individuals' in enc.columns:
        unique['n_individuals'] = by_qid.individuals.sum()
    unique = unique.fillna(0)

    aux[(0, 'unique')] = unique
    avg = unique.sum(axis='index') / len(quadrats)
    aux[(0, 'avg')] = avg

    indist_mask = enc.species.str.endswith(' sp.', na=True)
    indist_enc = indist_mask.sum()
    aux[(0, 'indist_enc')] = pd.Series({'N': indist_enc, '%': 100 * indist_enc / (indist_enc + len(enc.index))})

    if 'individuals' in enc.columns:
        indist_individuals = enc[indist_mask].individuals.sum()
        aux[(0, 'indist_indiv')] = pd.Series(
            {'N': indist_individuals, '%': 100 * indist_individuals / enc.individuals.sum()}
        )

    if 'direction' in enc.columns:
        dirdiff = direction_difference(enc).dropna()
        dirdiff['diff_abs#'] = dirdiff['diff_#'].abs()
        dirdiff = dirdiff.sort_values(by='diff_abs#', ascending=False)
        aux[(0, 'n_dir_diff_desc')] = describe_full(dirdiff)
        aux[(0, 'n_dir_diff')] = dirdiff

    unique = unique.reset_index(drop=False)
    return (unique.convert_dtypes(), aux)


def format_a1_aux(aux: A1Aux) -> str:
    """Format auxiliary results from a1 before writing to disk."""
    levels = sorted({k[0] for k in aux})
    res = []
    for lvl in levels:
        res += [
            f'====LEVEL {lvl} OVERVIEW START====',
            aux[(lvl, 'desc_full')].to_string(),
            '\n==Unique per quadrat==',
            aux[(lvl, 'unique')].to_string(),
            '\n==Avg per quadrat==',
            aux[(lvl, 'avg')].to_string(),
            '\n==Indistinguishable encounters==',
            aux[(lvl, 'indist_enc')].to_string(),
        ]

        if (lvl, 'indist_indiv') in aux:
            res += [
                '\n==Indistinguishable individuals==',
                aux[(lvl, 'indist_indiv')].to_string(),
            ]

        if (lvl, 'n_dir_diff') in aux:
            res += [
                '\n==Direction richness difference==',
                aux[(lvl, 'n_dir_diff_desc')].to_string(),
                '\n',
                aux[(lvl, 'n_dir_diff')].to_string(),
            ]

        res += [f'====LEVEL {lvl} OVERVIEW END====\n\n']
    return '\n'.join(res)


def a2_sar(
    spp_matrix: pd.DataFrame,
    analysis: Analysis2,
    sides_adj: Sides,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Return DataFrame containing quadrat richness accumulations.

    The accumulations are performed in random order and
    separate columns are added for slowest and fastest growing series
    and total accumulated area.
    """
    accumulated = []
    for _ in range(analysis.permutations):
        shuff = acc_richness_shuffled(spp_matrix)
        accumulated.append(shuff)
    sar = pd.DataFrame(accumulated).transpose()
    sar_add = agg_results(sar, 'n_species', 'area')

    min_acc = sar[0].to_list()
    max_acc = sar[0].to_list()
    for colname in sar:
        col = sar[colname].to_list()
        min_acc = min(col, min_acc)
        max_acc = max(col, max_acc)
    sar['min_acc'] = min_acc
    sar['max_acc'] = max_acc

    surface_area = sides_adj.x * sides_adj.y
    sar_add['area'] = sar['area'] = (sar.index + 1) * surface_area
    return sar, sar_add


def a3_species_delta_sum(
    spp_matrix: SppMatrix, dmat: pd.DataFrame, analysis: Analysis3
) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Return DataFrame containing distance-dependent species difference.

    Absolute difference between species lists is calculated
    for each pair of quadrats of given distance. This is done with all possible
    pairs of quadrats of given level.

    For relative comparison see `a7_jaccard_diss`.
    """
    deltas = species_delta_sum(spp_matrix, dmat)
    deltas = deltas[deltas.distance > 0].astype({'abs_diff': 'Int64'})
    deltas_stats = deltas.reset_index(drop=True)
    interval = analysis.interval
    if interval and not deltas.empty:
        bins = pd.interval_range(0, deltas.distance.max() + interval, freq=interval, closed='left')  # type: ignore[call-overload] # should accept numeric, but rejects float; pandas 2.2.0 bug?
        deltas['distance_bin'] = pd.cut(deltas.distance, bins)
        deltas_stats['distance'] = pd.cut(deltas_stats.distance, bins)
    deltas_stats = deltas_stats.groupby('distance', observed=False).agg(AGG_STATS)
    deltas_stats = flatten_multicolumns(deltas_stats).reset_index()
    return (deltas, deltas_stats)


def a4_extent_total(spp_matrix: SppMatrix, dmat: pd.DataFrame) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Return cardinality of a union of spp. in quadrats and auxiliary data grouped by analyzed extents.

    If a study grid that is linear on an x axis (e.g. a row of transects) is passed to this analysis,
    it returns a DataFrame with sum of richness of a pair of quadrats with their respective distances.

    :seealso: a4_rectangle()
    :seealso: a4_single()
    """
    max_valid_coord = min(spp_matrix.index.get_level_values(f'coord_{axis}').max() for axis in ('x', 'y'))
    if max_valid_coord > 0:
        subgrid_richness = a4_rectangle(spp_matrix, dmat, max_valid_coord)
    else:
        subgrid_richness = a4_single(spp_matrix, dmat)

    stat_cols = subgrid_richness[['spp_total', 'spp_mean', 'extent']]
    stat_cols['extent'] = stat_cols.extent.round(2)
    stats = stat_cols.groupby('extent', observed=False).agg(AGG_STATS)
    stats = flatten_multicolumns(stats).reset_index()
    return (subgrid_richness, stats)


def a4_rectangle(spp_matrix: SppMatrix, dmat: pd.DataFrame, max_valid_coord: int) -> pd.DataFrame:
    """Return cardinality of a union of spp. in quadrats positioned in rectangular pattern.

    :param spp_matrix: species matrix
    :param max_valid_coord: maximum permissible length of created rectangle in quadrats
    :returns: a4 result for rectangular grids
    """
    res: dict[str, list[float]]
    res = {'spp_total': [], 'extent': []}
    for q_x, q_y in spp_matrix.index:
        for shift in range(1, max_valid_coord + 1):
            quartet = [(q_x, q_y), (q_x + shift, q_y), (q_x, q_y + shift), (q_x + shift, q_y + shift)]
            is_valid = all(q in spp_matrix.index for q in quartet)
            if not is_valid:
                continue
            spp_union = np.count_nonzero(spp_matrix.loc[quartet].any())
            res['spp_total'].append(spp_union)
            res['extent'].append(dmat.at[(*quartet[0], *quartet[3]), 'distance'])
    subgrid_richness = pd.DataFrame(res)
    subgrid_richness['spp_mean'] = subgrid_richness.spp_total / 4
    return subgrid_richness


def a4_single(spp_matrix: SppMatrix, dmat: pd.DataFrame) -> pd.DataFrame:
    """Return cardinality of a union of spp. in quadrats forming endpoints of a line segment.

    :param spp_matrix: species matrix
    :param dmat: distance matrix
    :returns: a4 result for single-row or single-column grids
    """
    res: dict[str, list[float]]
    res = {'spp_total': [], 'extent': []}
    for i, q1 in enumerate(spp_matrix.itertuples()):
        for q2 in spp_matrix.iloc[i + 1 :].itertuples():
            spp_union = np.count_nonzero(np.logical_or(q1[1:], q2[1:]))
            res['spp_total'].append(spp_union)
            res['extent'].append(dmat.at[(*q1.Index, *q2.Index), 'distance'])  # type: ignore[misc] # .Index is a tuple
    subgrid_richness = pd.DataFrame(res)
    subgrid_richness['spp_mean'] = subgrid_richness.spp_total / 2
    return subgrid_richness


def a5_observed_expected(spp_matrix: SppMatrix) -> tuple[pd.DataFrame, None]:
    """Return DataFrame containing ratio of variance of observed vs expected variance of species in subgrids of a grid represented by a given species matrix."""
    max_x, max_y = (spp_matrix.index.get_level_values(c).max() for c in ('coord_x', 'coord_y'))
    max_gap_x, max_gap_y = ((c - 3) // 3 for c in (max_x, max_y))
    if max_gap_x < 0:
        raise UnsuitableGridError('a5', None, 'x', 'grid must be at least 4 columns wide')
    create_subgrid = create_quadruple

    max_gap = max_gap_x
    single_row = max_y == 0
    if not single_row:
        if max_gap_y < 0:
            raise UnsuitableGridError('a5', None, 'y', 'grid must be at least 4 rows high')
        create_subgrid = create_sexdecuple
        max_gap = min(max_gap_x, max_gap_y)

    subgrid_oe_ratios: dict[str, list[float]]
    subgrid_oe_ratios = {'variance_ratio': [], 'gap': []}
    for gap in range(max_gap + 1):
        for q in spp_matrix.index:
            subgrid_idx = create_subgrid(q, gap)
            is_valid = all(q in spp_matrix.index for q in subgrid_idx)
            if not is_valid:
                continue
            spp_mat_subgrid = spp_matrix.loc[subgrid_idx]
            species_proportion = spp_mat_subgrid.sum(axis='index') / len(spp_mat_subgrid)  # p_i in PaW paper
            expected_var = (species_proportion * (1 - species_proportion)).sum()
            observed_var = spp_mat_subgrid.sum(axis='columns').var(ddof=0)
            subgrid_oe_ratios['variance_ratio'].append(observed_var / expected_var)
            subgrid_oe_ratios['gap'].append(gap)
    return pd.DataFrame(subgrid_oe_ratios), None


def a6_shared_ratios(spp_matrix: SppMatrix, dmat: pd.DataFrame) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Return DataFrame containing various ratios of quadrat spp. intersection with regards to distance.

    Specifically spp. intersection vs
    species total, vs. union of species and between total richness of quadrats in the entire study grid.

    Auxiliary data is returned as an aggregation of various statistics
    for said DataFrame.
    """
    res: dict[str, list[pd.Series[float]]] = {
        'common_diff': [],
        'common_total': [],
        'common_union': [],
        'distance': [],
    }
    spp_grid_total = len(spp_matrix.columns)
    for idx, row in spp_matrix.iterrows():
        n_intersect = (spp_matrix & row).sum(axis='columns')
        n_xor = (spp_matrix ^ row).sum(axis='columns')
        n_union = (spp_matrix | row).sum(axis='columns')

        res['common_diff'].append(n_intersect / n_xor)
        res['common_total'].append(n_intersect / spp_grid_total)
        res['common_union'].append(n_intersect / n_union)
        res['distance'].append(dmat.loc[idx, 'distance'])  # type: ignore[arg-type, index]   # is truly pd.Series, can be indexed by MultiIndex tuple just fine
    res = {k: pd.concat(v).reset_index(drop=True) for k, v in res.items()}
    ratios = pd.DataFrame(res)
    ratios = ratios[ratios.distance > 0]
    with np.errstate(invalid='ignore'):  # inf-inf may occur since values above may be inf
        ratios_stats = ratios.groupby('distance').agg(AGG_STATS)
    ratios_stats = flatten_multicolumns(ratios_stats).reset_index()
    return ratios, ratios_stats


def a7_jaccard_diss(spp_matrix: pd.DataFrame, dmat: pd.DataFrame) -> tuple[pd.DataFrame, None]:
    """Return Jaccard dissimilarity for respective distances in a DataFrame.

    >>> spp_idx = pd.MultiIndex.from_arrays([[0, 0, 1], [0, 1, 0]], names=('coord_x', 'coord_y'))
    >>> spp_matrix = pd.DataFrame(
    ...     index=spp_idx,
    ...     data={
    ...         'A': [True, True, False],
    ...         'B': [True, False, False],
    ...         'C': [True, False, False],
    ...         'D': [False, True, False],
    ...     },
    ... )
    >>> idx = pd.MultiIndex.from_tuples(
    ...     [
    ...         (0, 0, 0, 0),
    ...         (0, 0, 0, 1),
    ...         (0, 0, 1, 0),
    ...         (0, 1, 0, 0),
    ...         (0, 1, 0, 1),
    ...         (0, 1, 1, 0),
    ...         (1, 0, 0, 0),
    ...         (1, 0, 0, 1),
    ...         (1, 0, 1, 0),
    ...     ],
    ...     names=['coord_x', 'coord_y', 'coord_x_other', 'coord_y_other'],
    ... )
    >>> dmat = pd.DataFrame(index=idx, data={'distance': [0, 1, 1, 1, 0, 2, 1, 2, 0]})
    >>> a7_jaccard_diss(spp_matrix, dmat)[0]
       jaccard_dissimilarity  log_jaccard_dissimilarity  distance
    1                   0.75                  -0.124939         1
    2                   1.00                   0.000000         1
    3                   0.75                  -0.124939         1
    5                   1.00                   0.000000         2
    6                   1.00                   0.000000         1
    7                   1.00                   0.000000         2
    """
    jacc_diss = []
    dists = []
    for idx, row in spp_matrix.iterrows():
        n_intersect = (spp_matrix & row).sum(axis='columns')
        n_unique_a = (spp_matrix & ~row).sum(axis='columns')
        n_unique_b = (~spp_matrix & row).sum(axis='columns')

        divisor = n_intersect + n_unique_a + n_unique_b
        jd = 1 - (n_intersect / divisor)

        jacc_diss.append(jd)
        dists.append(dmat.loc[idx, 'distance'])  # type: ignore[index]   # can be indexed by MultiIndex tuple just fine
    jd_series = pd.concat(jacc_diss, ignore_index=True)
    with np.errstate(divide='ignore'):  # J.d. may be 0 for 2 quadrats with same species, results in NaN
        log_jd = np.log10(jd_series)
    jaccard = pd.DataFrame(
        {
            'jaccard_dissimilarity': jd_series,
            'log_jaccard_dissimilarity': log_jd,
            'distance': pd.concat(dists, ignore_index=True),
        }
    )
    jaccard = jaccard[jaccard.distance > 0]
    return jaccard, None


def a8_abundance(grid: GridInfo) -> tuple[pd.DataFrame, None]:
    """Return DataFrame containing equitability of abundance and species abundance distribution data."""
    spp_totals = grid.enc[['individuals', 'species']].groupby('species', as_index=False).sum()
    bins_scalars = geom_r2(1, spp_totals.individuals.max() * 2)
    spp_totals['individuals_bin'] = pd.cut(spp_totals.individuals, bins_scalars, right=False)
    # prevent pandas.errors.IntCastingNaNError: Cannot convert non-finite values (NA or inf) to integer during pd.concat()
    # pandas bug as of 2.2.1?
    spp_totals['individuals_bin'] = spp_totals['individuals_bin'].astype(str)
    return spp_totals, None


def check_grid_suitability(qlist: QList, strat: LevelStrategy, levels: list[int]) -> None:
    """Throw exception if grid is not suitable for analysis with chosen parameters."""
    max_lvl = max(levels)
    if strat == 'nested-quadrats':
        axes: Final[list[Literal]] = ['x', 'y']  # make mypy happy
        for axis in axes:
            dim = qlist.index.get_level_values(f'coord_{axis}').max() + 1
            max_q_size = 2 ** (max_lvl - 1)
            if dim % max_q_size != 0 and max_lvl > 1:
                problem_levels = [lvl for lvl in levels if lvl > 1]
                raise UnsuitableGridError(
                    strat, problem_levels, axis, 'axis dimensions must be tileable by merged quadrats'
                )
            if dim < max_q_size:
                problem_levels = [lvl for lvl in levels if 2 ** (lvl - 1) > dim]
                raise UnsuitableGridError(
                    strat, problem_levels, axis, f'axis dimension {dim} too small to accommodate quadrat merging'
                )
    elif strat == 'striped-transect-merging':
        dim = qlist.index.get_level_values('coord_x').max() + 1
        if dim % max_lvl != 0:
            raise UnsuitableGridError(strat, [max_lvl], 'x', 'axis dimension must be divisible by maximum level')
    elif strat == 'repeated-transect-merging':
        dim = qlist.index.get_level_values('coord_x').max() + 1
        problem_levels = [lvl for lvl in levels if dim % lvl != 0]
        if problem_levels:
            raise UnsuitableGridError(strat, [max_lvl], 'x', 'axis dimension must be divisible by every level')


def adjust_for_lvl(
    grid: GridInfo, lvl: int, max_lvl: int, lvl_strat: LevelStrategy, sides: Sides
) -> tuple[GridInfo, Sides]:
    """Transform study grid according to current level and level strategy."""
    enc, quadrats = grid.enc, grid.qlist
    if lvl_strat == 'repeated-transect-merging':
        grid, sides_adj = adjust_repeated_transect_merging(enc, quadrats, sides, lvl)
    elif lvl_strat == 'striped-transect-merging':
        grid, sides_adj = adjust_striped_transect_merging(enc, quadrats, sides, lvl, max_lvl)
    elif lvl_strat == 'nested-quadrats':
        grid, sides_adj = adjust_nested_quadrats(enc, quadrats, sides, lvl, max_lvl)
    else:
        raise conf.InvalidTaskError(lvl_strat)

    check_grid_consistency(grid)
    return (grid, sides_adj)


def check_grid_consistency(grid: GridInfo, *, post_adj: bool = False) -> None:
    enc, quadrats = grid.enc, grid.qlist
    enc = enc.set_flags(allows_duplicate_labels=False)
    quadrats = quadrats.set_flags(allows_duplicate_labels=False)
    qids_enc = set(zip(enc.coord_x, enc.coord_y, strict=False))
    qids_qlist = set(
        zip(quadrats.index.get_level_values('coord_x'), quadrats.index.get_level_values('coord_y'), strict=False)
    )
    qids_diff = qids_enc - qids_qlist
    if len(qids_diff) > 0:
        raise InvalidQuadratsError(qids_diff, post_adj=post_adj)
    if quadrats.isna().any().any():
        raise MissingValuesError(quadrats[quadrats.isna().any(axis='columns')], post_adj=post_adj)
    enc_subset = enc[['species', 'coord_x', 'coord_y']]
    if enc_subset.isna().any().any():
        raise MissingValuesError(enc_subset[enc_subset.isna().any(axis='columns')], post_adj=post_adj)


def adjust_repeated_transect_merging(enc: Encounters, qlist: QList, sides: Sides, level: int) -> tuple[GridInfo, Sides]:
    """Adjust passed encounters, quadrat list and sides for repeated-transect-merging strategy at given `level`.

    >>> enc = pd.DataFrame(
    ...     {
    ...         'coord_x': [0, 1, 1, 1, 3],
    ...         'coord_y': [0, 0, 1, 2, 3],
    ...         'species': ['B', 'A', 'A', 'C', 'B'],
    ...     }
    ... )
    >>> qlist = pd.DataFrame(
    ...     index=pd.MultiIndex.from_arrays([[0, 0, 1, 1, 1, 3], [0, 1, 0, 1, 2, 3]], names=('coord_x', 'coord_y')),
    ...     data={'centroid_x': [2, 2, 6, 6, 10, 14], 'centroid_y': [2, 6, 2, 6, 10, 14]},
    ... )
    >>> sides = Sides(4, 4)
    >>> res = adjust_repeated_transect_merging(enc, qlist, sides, level=2)
    >>> res[0].enc
       coord_x  coord_y species
    0        0        0       B
    1        0        0       A
    2        0        1       A
    3        0        2       C
    4        1        3       B
    >>> res[0].qlist  # doctest: +NORMALIZE_WHITESPACE
                     centroid_x  centroid_y
    coord_x coord_y
    0       0               6.0           2
            1               6.0           6
            2              14.0          10
    1       3              18.0          14
    >>> res[1]
    Sides(x=8.0, y=4.0)
    """
    new_xs = enc.coord_x.floordiv(level)
    enc = copy.deepcopy(enc)
    enc.loc[:, 'coord_x'] = new_xs

    new_xs = qlist.index.get_level_values('coord_x').to_series().floordiv(level)
    new_idx = pd.MultiIndex.from_arrays([new_xs, qlist.index.get_level_values('coord_y')], names=('coord_x', 'coord_y'))
    qlist = qlist.set_index(new_idx)
    qlist = qlist[~qlist.index.duplicated()]
    qlist['centroid_x'] = qlist['centroid_x'] + ((sides.x / 2) * level)

    return GridInfo(enc, qlist), Sides(sides.x * level, sides.y)


def adjust_striped_transect_merging(
    enc: Encounters, qlist: QList, sides: Sides, level: int, max_level: int
) -> tuple[GridInfo, Sides]:
    """Adjust passed grid info list and sides for striped-transect-merging strategy at given `level` and `max_level`.

    >>> enc = pd.DataFrame(
    ...     {
    ...         'coord_x': [0, 1, 1, 1, 3],
    ...         'coord_y': [0, 0, 1, 2, 3],
    ...         'species': ['B', 'A', 'A', 'C', 'B'],
    ...     }
    ... )
    >>> qlist = pd.DataFrame(
    ...     index=pd.MultiIndex.from_arrays([[0, 0, 1, 1, 1, 3], [0, 1, 0, 1, 2, 3]], names=('coord_x', 'coord_y')),
    ...     data={'centroid_x': [2, 2, 6, 6, 10, 14], 'centroid_y': [2, 6, 2, 6, 10, 14]},
    ... )
    >>> sides = Sides(4, 4)
    >>> res = adjust_striped_transect_merging(enc, qlist, sides, level=1, max_level=2)
    >>> res[0].enc
       coord_x  coord_y species
    0        0        0       B
    >>> res[0].qlist  # doctest: +NORMALIZE_WHITESPACE
                     centroid_x  centroid_y
    coord_x coord_y
    0       0               4.0           2
            1               4.0           6
    >>> res[1]
    Sides(x=4.0, y=4.0)
    """
    enc = enc[(enc.coord_x % max_level) < level]
    new_xs = enc.coord_x.floordiv(max_level)
    enc.loc[:, 'coord_x'] = new_xs

    q_xs = qlist.index.get_level_values('coord_x')
    qlist = qlist[(q_xs % max_level) < level]
    q_xs = qlist.index.get_level_values('coord_x')
    new_xs = q_xs.to_series().floordiv(max_level)
    new_idx = pd.MultiIndex.from_arrays([new_xs, qlist.index.get_level_values('coord_y')], names=('coord_x', 'coord_y'))
    qlist = qlist.set_index(new_idx)

    qlist = qlist[~qlist.index.duplicated()]

    qlist['centroid_x'] = qlist['centroid_x'] + ((sides.x / 2) * level)

    return GridInfo(enc, qlist), Sides(sides.x * level, sides.y)


def adjust_nested_quadrats(
    enc: Encounters, qlist: QList, sides: Sides, level: int, max_level: int
) -> tuple[GridInfo, Sides]:
    """Adjust passed grid information for nested-quadrats strategy at given `level` and `max_level`."""
    merge_n_quadrats = 2 ** (level - 1)
    retain_every_nth = 2 ** (max_level - level)
    adj_sides = Sides(sides.x * merge_n_quadrats, sides.y * merge_n_quadrats)

    new_xs = enc.coord_x.floordiv(merge_n_quadrats)
    new_ys = enc.coord_y.floordiv(merge_n_quadrats)
    enc = enc.assign(coord_x=new_xs, coord_y=new_ys)
    keep_x = (enc.coord_x % retain_every_nth) == 0
    keep_y = (enc.coord_y % retain_every_nth) == 0
    enc = enc[keep_x & keep_y]
    enc = enc.assign(coord_x=new_xs // retain_every_nth, coord_y=new_ys // retain_every_nth)

    qlist = copy.deepcopy(qlist)
    qlist['coord_x'] = qlist.index.get_level_values('coord_x')
    qlist['coord_y'] = qlist.index.get_level_values('coord_y')

    new_xs = qlist.coord_x.floordiv(merge_n_quadrats)
    new_ys = qlist.coord_y.floordiv(merge_n_quadrats)
    qlist = qlist.assign(coord_x=new_xs, coord_y=new_ys)

    keep_x = (qlist.coord_x % retain_every_nth) == 0
    keep_y = (qlist.coord_y % retain_every_nth) == 0
    qlist = qlist[keep_x & keep_y]

    qlist['centroid_x'] = (qlist.coord_x * adj_sides.x) + (adj_sides.x / 2)
    qlist['centroid_y'] = (qlist.coord_y * adj_sides.y) + (adj_sides.y / 2)
    qlist = qlist.assign(coord_x=qlist.coord_x // retain_every_nth, coord_y=qlist.coord_y // retain_every_nth)
    qlist = qlist.drop_duplicates(subset=['coord_x', 'coord_y'])
    qlist = qlist.set_index(['coord_x', 'coord_y'], drop=True, verify_integrity=True)

    return GridInfo(enc, qlist), adj_sides


def get_lvl_infos(grinfo: GridInfo, sides_adj: Sides, cfg: conf.Config) -> dict[int, LevelInfo]:
    infos = {}
    for level in sorted(set(cfg.levels)):
        grid_adj, sides_adj_lvl = adjust_for_lvl(
            grinfo,
            level,
            max(cfg.levels),
            cfg.level_strategy,
            sides_adj,
        )
        dist_fn = get_distance_function(cfg.distance_type, sides_adj_lvl)
        dmat = distance_matrix(grid_adj.qlist, dist_fn)
        infos[level] = copy.deepcopy(LevelInfo(grid_adj, dmat, sides_adj_lvl))
    return infos


def save_results(prefix: str, out_dir: Path, info: tuple[str, int], result: AnalysisResult) -> None:
    """Save analysis results to `out_dir`.

    :param prefix: filename fragment to prefix analyses types with for saved results
    :param out_dir: directory to save results into. Must be created beforehand
    :param result: result to save
    """
    out_file = analysis_results_filename(out_dir, prefix, info)
    result.res.to_csv(out_file, index=False)
    logger.info(f'result written to {out_file}')

    if result.aux is None:
        return
    out_file = out_file.with_stem(f'{out_file.stem}-aux')

    if isinstance(result.aux, pd.DataFrame):
        out_file = out_file.with_suffix('.csv')
        result.aux.to_csv(out_file, index=False)
    else:
        out_file = out_file.with_suffix('.txt')
        out_file.write_text(format_a1_aux(result.aux), encoding='utf-8')
    logger.info(f'auxiliary data written to {out_file}')


def create_sexdecuple(q: Coord, gap: int) -> list[Coord]:
    """Return sexdecuple (16) of quadrat coordinates constituting a rectangular subgrid.

    >>> create_sexdecuple((0, 0), 0)
    [(0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3), (2, 0), (2, 1), (2, 2), (2, 3), (3, 0), (3, 1), (3, 2), (3, 3)]
    >>> create_sexdecuple((1, 1), 1)
    [(1, 1), (1, 3), (1, 5), (1, 7), (3, 1), (3, 3), (3, 5), (3, 7), (5, 1), (5, 3), (5, 5), (5, 7), (7, 1), (7, 3), (7, 5), (7, 7)]
    """
    xs = (q[0] + x * (gap + 1) for x in range(4))
    ys = [q[1] + y * (gap + 1) for y in range(4)]
    return [(x, y) for x in xs for y in ys]


def create_quadruple(q: Coord, gap: int) -> list[Coord]:
    """Return quadruple of equidistant quadrat coordinates in a row.

    >>> create_quadruple((0, 0), 0)
    [(0, 0), (1, 0), (2, 0), (3, 0)]
    >>> create_quadruple((1, 0), 1)
    [(1, 0), (3, 0), (5, 0), (7, 0)]
    """
    xs = (q[0] + x * (gap + 1) for x in range(4))
    return [(x, 0) for x in xs]


def flatten_multicolumns(df: pd.DataFrame) -> pd.DataFrame:
    """Flatten and rename multicolumns in a DataFrame.

    >>> cols = pd.MultiIndex.from_tuples([('top', 'btm1'), ('top', 'btm2')])
    >>> multi = pd.DataFrame(data=[[0, 1], [2, 3]], columns=cols)
    >>> flatten_multicolumns(multi)
       top_btm1  top_btm2
    0         0         1
    1         2         3
    """
    df.columns = df.columns.to_flat_index()  # type: ignore[no-untyped-call] # is typed, mypy 1.9.0 bug?
    name_map = {col: f'{col[0]}_{col[1]}' for col in df.columns}
    df = df.rename(name_map, axis='columns', errors='raise')
    return df


def get_distance_function(dist_type: Distance, sides_adj: Sides) -> DistFunction:
    """Return function to use for computing values in distance matrix.

    >>> sides = Sides(1, 1)
    >>> get_distance_function('euclid-diagonal', sides)  # doctest: +ELLIPSIS
    <function get_distance_function.<locals>.<lambda> at ...>
    >>> get_distance_function(':)', sides)  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
    seal.exceptions.InvalidLiteralError: argument dist_type=':)' is not a valid choice
    """
    if dist_type == 'euclid-centroid':
        return lambda ps, qs: q_distance_centroid(ps, qs)
    if dist_type == 'chebyshev':
        return lambda ps, qs: q_distance_chebyshev(ps, qs, sides_adj)
    if dist_type == 'euclid-diagonal':
        return lambda ps, qs: q_distance_diagonal(ps, qs, sides_adj)
    raise InvalidLiteralError(f'{dist_type=}')


def get_spp_matrix(grinfo: GridInfo) -> SppMatrix:
    """Return DataFrame with species matrix for passed *grinfo*.

    The species matrix has MultiIndex coord_x, coord_y labels with quadrat coordinates
    and species as column names with bool values denoting whether species COL was encountered in quadrat LABEL.

    >>> enc = pd.DataFrame(
    ...     {'coord_x': [0, 1, 1, 1, 1], 'coord_y': [0, 0, 1, 0, 1], 'species': ['B', 'A', 'A', 'C', 'B']}
    ... )
    >>> idx = pd.MultiIndex.from_arrays([[0, 0, 1, 1, 2], [0, 1, 0, 1, 0]], names=('coord_x', 'coord_y'))
    >>> quadrats = pd.DataFrame(index=idx)
    >>> get_spp_matrix(GridInfo(enc, quadrats))  # doctest: +NORMALIZE_WHITESPACE
    species              A      B      C
    coord_x coord_y
    0       0        False   True  False
            1        False  False  False
    1       0         True  False   True
            1         True   True  False
    2       0        False  False  False
    """
    enc, quadrats = grinfo.enc, grinfo.qlist
    enccoords = [enc.coord_x, enc.coord_y]
    spp_matrix = pd.crosstab(enccoords, columns=enc.species, dropna=False)
    spp_matrix = spp_matrix.reindex(quadrats.index).fillna(0).astype(bool)
    spp_matrix = spp_matrix.sort_index()
    return spp_matrix


def p025(x: 'pd.Series[float]') -> float:
    """Return 0.25 quantile (25th percentile) of pandas Series."""
    return x.quantile(0.025)


def p975(x: 'pd.Series[float]') -> float:
    """Return 0.975 quantile (97.5th percentile) of pandas Series."""
    return x.quantile(0.975)


AGG_STATS: list[str | Callable[['pd.Series[float]'], float]] = [
    'mean',
    'median',
    'sem',
    'std',
    'count',
    'min',
    'max',
    'skew',
    p025,
    p975,
]
"""List with aggregation to be called at once for convenience."""


def acc_richness_shuffled(spp_matrix: pd.DataFrame) -> 'pd.Series[int]':
    """Return Series containing accumulated richness.

    Quadrats are accumulated in random order.

    >>> np.random.seed(0)
    >>> spp_matrix = pd.DataFrame(
    ...     {
    ...         'A': [True, False, True, False],
    ...         'B': [True, False, False, False],
    ...         'C': [True, True, False, False],
    ...         'D': [False, False, False, False],
    ...         'E': [False, False, True, False],
    ...     }
    ... )
    >>> acc_richness_shuffled(spp_matrix)
    0    2
    1    2
    2    3
    3    4
    dtype: int64
    """
    return (
        spp_matrix.sample(frac=1, ignore_index=True)  # reshuffle dataset
        .cumsum()
        .astype(bool)
        .sum(axis='columns')  # sum total species
    )


def agg_results(df: pd.DataFrame, col_prefix: str, index_name: str) -> pd.DataFrame:
    """Aggregate data from DataFrame in a wide format.

    >>> pd.set_option('display.width', None)
    >>> df = pd.DataFrame({'col1': [1, 0], 'col2': [2, 1], 'col3': [2, 2], 'col4': [1, 3]})
    >>> agg_results(df, 'col', 'idx')  # doctest: +NORMALIZE_WHITESPACE
         col_mean  col_med   col_sem   col_std  col_count  col_p025  col_p975  col_min  col_max  col_skew  col_kurtosis
    idx
    0         1.5      1.5  0.288675  0.577350          4     1.000     2.000        1        2       0.0          -6.0
    1         1.5      1.5  0.645497  1.290994          4     0.075     2.925        0        3       0.0          -1.2
    """
    res = pd.DataFrame(index=df.index)
    res.index.name = index_name
    res[f'{col_prefix}_mean'] = df.mean(axis='columns')
    res[f'{col_prefix}_med'] = df.median(axis='columns')
    res[f'{col_prefix}_sem'] = df.sem(axis='columns')
    res[f'{col_prefix}_std'] = df.std(axis='columns')
    res[f'{col_prefix}_count'] = df.count(axis='columns')
    res[f'{col_prefix}_p025'] = df.quantile(0.025, axis='columns')
    res[f'{col_prefix}_p975'] = df.quantile(0.975, axis='columns')
    res[f'{col_prefix}_min'] = df.min(axis='columns')
    res[f'{col_prefix}_max'] = df.max(axis='columns')
    res[f'{col_prefix}_skew'] = df.skew(axis='columns')
    res[f'{col_prefix}_kurtosis'] = df.kurtosis(axis='columns')
    return res


def describe_full(df: pd.DataFrame) -> pd.DataFrame:
    stats = df.describe(percentiles=[0.025, 0.25, 0.5, 0.75, 0.975], include='all')
    stats.loc['median'] = df.median(numeric_only=True)
    stats.loc['sem'] = df.sem(numeric_only=True)
    stats.loc['skewness'] = df.skew(numeric_only=True)
    stats.loc['kurtosis'] = df.kurtosis(numeric_only=True)
    return stats


def species_delta_sum(species: SppMatrix, dmat: pd.DataFrame) -> pd.DataFrame:
    """Return DataFrame containing absolute in quadrats richness.

    >>> cols = ['A', 'B', 'C', 'D', 'E']
    >>> vals = [
    ...     [True, True, True, False, False],
    ...     [True, False, True, False, False],
    ...     [False, False, False, False, True],
    ... ]
    >>> idx_spp = pd.MultiIndex.from_tuples([(0, 0), (0, 1), (1, 0)])
    >>> spp_matrix = pd.DataFrame(index=idx_spp, data=vals, columns=cols)
    >>> idx = pd.MultiIndex.from_tuples(
    ...     [
    ...         (0, 0, 0, 0),
    ...         (0, 0, 0, 1),
    ...         (0, 0, 1, 0),
    ...         (0, 1, 0, 0),
    ...         (0, 1, 0, 1),
    ...         (0, 1, 1, 0),
    ...         (1, 0, 0, 0),
    ...         (1, 0, 0, 1),
    ...         (1, 0, 1, 0),
    ...     ],
    ...     names=['coord_x', 'coord_y', 'coord_x_other', 'coord_y_other'],
    ... )
    >>> dmat = pd.DataFrame(index=idx, data={'distance': [0, 1, 1, 1, 0, 2, 1, 2, 0]})
    >>> species_delta_sum(spp_matrix, dmat)
         distance  abs_diff  spp_union
    0 0         0         0          3
      1         1         0          3
    1 0         1         1          4
    0 0         1         1          3
      1         0         0          2
    1 0         2         1          3
    0 0         1         3          4
      1         2         2          3
    1 0         0         0          1

    >>> vals = [
    ...     [False, False, False, False, False],
    ...     [True, False, True, False, False],
    ...     [False, False, False, False, False],
    ... ]
    >>> spp_matrix = pd.DataFrame(index=idx_spp, data=vals, columns=cols)
    >>> species_delta_sum(spp_matrix, dmat)
         distance  abs_diff  spp_union
    0 0         0         0          0
      1         1         2          2
    1 0         1         0          0
    0 0         1         0          2
      1         0         0          2
    1 0         2         0          2
    0 0         1         0          0
      1         2         2          2
    1 0         0         0          0
    """
    spp_diff_sum: dict[str, list[pd.Series[float]]] = {'distance': [], 'abs_diff': [], 'spp_union': []}
    for idx, row in species.iterrows():
        abs_diffs = (species & ~row).sum(axis='columns')
        spp_union = (species | row).sum(axis='columns')
        spp_diff_sum['abs_diff'].append(abs_diffs)
        spp_diff_sum['spp_union'].append(spp_union)
        dists = dmat.loc[idx, 'distance']  # type: ignore[index]   # can be indexed by MultiIndex tuple just fine
        spp_diff_sum['distance'].append(dists)  # type: ignore[arg-type]   # is Series, not DF, verified by assert
    res = pd.DataFrame(
        {
            'distance': pd.concat(spp_diff_sum['distance']),
            'abs_diff': pd.concat(spp_diff_sum['abs_diff']),
            'spp_union': pd.concat(spp_diff_sum['spp_union']),
        }
    )
    return res


def distance_matrix(quadrats: pd.DataFrame, distance: DistFunction) -> pd.DataFrame:
    """Convert quadrat's grid coordinates to quadrat's centroid coordinates on a meters-based plane.

    >>> df = pd.DataFrame(
    ...     {
    ...         'coord_x': [0, 0, 1, 1],
    ...         'coord_y': [0, 1, 0, 1],
    ...         'centroid_x': [0.5, 0.5, 1.5, 1.5],
    ...         'centroid_y': [0.5, 1.5, 0.5, 1.5],
    ...     }
    ... )
    >>> distance_matrix(df, lambda a, b: q_distance_diagonal(a, b, Sides(1, 1)))  # doctest: +NORMALIZE_WHITESPACE
                                                 distance
    coord_x coord_y coord_x_other coord_y_other
    0       0       0             0              0.000000
                                  1              2.236068
                    1             0              2.236068
                                  1              2.828427
            1       0             0              2.236068
                                  1              0.000000
                    1             0              2.828427
                                  1              2.236068
    1       0       0             0              2.236068
                                  1              2.828427
                    1             0              0.000000
                                  1              2.236068
            1       0             0              2.828427
                                  1              2.236068
                    1             0              2.236068
                                  1              0.000000
    """
    quadrats = quadrats.reset_index(drop=False)
    dmat = quadrats.join(quadrats, how='cross', rsuffix='_other')
    ps = dmat[['centroid_x', 'centroid_y']]
    qs = dmat[['centroid_x_other', 'centroid_y_other']]
    qs = qs.rename(columns={'centroid_x_other': 'centroid_x', 'centroid_y_other': 'centroid_y'}, errors='raise')
    dmat = dmat[['coord_x', 'coord_y', 'coord_x_other', 'coord_y_other']]
    dmat['distance'] = distance(ps, qs)
    dmat = dmat.set_index(['coord_x', 'coord_y', 'coord_x_other', 'coord_y_other'])
    dmat = dmat.sort_index()
    return dmat


def q_distance_centroid(q1: pd.DataFrame, q2: pd.DataFrame) -> 'pd.Series[float]':
    """Return series of Euclidean distance between centroids of quadrats at `q1` and `q2`.

    Grid coords should be paired-up, e.g. using cross-join, beforehand.

    >>> q1 = pd.DataFrame(
    ...     {
    ...         'centroid_x': [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5],
    ...         'centroid_y': [0.5, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5, 0.5, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5],
    ...     }
    ... )
    >>> q2 = pd.DataFrame(
    ...     {
    ...         'centroid_x': [0.5, 0.5, 1.5, 1.5, 0.5, 0.5, 1.5, 1.5, 0.5, 0.5, 1.5, 1.5, 0.5, 0.5, 1.5, 1.5],
    ...         'centroid_y': [0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5],
    ...     }
    ... )
    >>> q_distance_centroid(q1, q2)
    0     0.000000
    1     1.000000
    2     1.000000
    3     1.414214
    4     1.000000
    5     0.000000
    6     1.414214
    7     1.000000
    8     1.000000
    9     1.414214
    10    0.000000
    11    1.000000
    12    1.414214
    13    1.000000
    14    1.000000
    15    0.000000
    dtype: float64
    """
    dists = pd.Series(np.sqrt((q1.centroid_x - q2.centroid_x) ** 2 + (q1.centroid_y - q2.centroid_y) ** 2))
    return cast('pd.Series[float]', dists)


def q_distance_chebyshev(q1: pd.DataFrame, q2: pd.DataFrame, sides: Sides) -> 'pd.Series[float]':
    """Return series of farthest single direction side-to-side distances between dataframes of points at `q1` and `q2`.

    Extent as per P&W paper, longest distance on 1 axis, taking larger of x or y differences.

    Correction is applied to result to make distance of quadrat to itself 0.

    Grid coords should be paired-up, e.g. using cross-join, beforehand.

    >>> q1 = pd.DataFrame(
    ...     {
    ...         'centroid_x': [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5],
    ...         'centroid_y': [0.5, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5, 0.5, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5],
    ...     }
    ... )
    >>> q2 = pd.DataFrame(
    ...     {
    ...         'centroid_x': [0.5, 0.5, 1.5, 1.5, 0.5, 0.5, 1.5, 1.5, 0.5, 0.5, 1.5, 1.5, 0.5, 0.5, 1.5, 1.5],
    ...         'centroid_y': [0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5],
    ...     }
    ... )
    >>> q_distance_chebyshev(q1, q2, Sides(1, 1))
    0     0.0
    1     2.0
    2     2.0
    3     2.0
    4     2.0
    5     0.0
    6     2.0
    7     2.0
    8     2.0
    9     2.0
    10    0.0
    11    2.0
    12    2.0
    13    2.0
    14    2.0
    15    0.0
    Name: centroid_x, dtype: float64
    """
    x_diff = (q1.centroid_x - q2.centroid_x).abs()
    y_diff = (q1.centroid_y - q2.centroid_y).abs()
    x_diff[x_diff > 0] += sides.x
    y_diff[y_diff > 0] += sides.y
    dist = x_diff.where(x_diff > y_diff, y_diff)
    return cast('pd.Series[float]', dist)


def q_distance_diagonal(q1: pd.DataFrame, q2: pd.DataFrame, sides: Sides) -> 'pd.Series[float]':
    """Return series of farthest corner-to-corner distances between quadrats in list `q1` and `q2`, row-wise.

    Quadrats should be paired-up beforehand, e.g. using cross join.

    >>> q1 = pd.DataFrame(
    ...     {
    ...         'centroid_x': [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5],
    ...         'centroid_y': [0.5, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5, 0.5, 0.5, 0.5, 0.5, 1.5, 1.5, 1.5, 1.5],
    ...     }
    ... )
    >>> q2 = pd.DataFrame(
    ...     {
    ...         'centroid_x': [0.5, 0.5, 1.5, 1.5, 0.5, 0.5, 1.5, 1.5, 0.5, 0.5, 1.5, 1.5, 0.5, 0.5, 1.5, 1.5],
    ...         'centroid_y': [0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5, 0.5, 1.5],
    ...     }
    ... )
    >>> q_distance_diagonal(q1, q2, Sides(1, 1))
    0     0.000000
    1     2.236068
    2     2.236068
    3     2.828427
    4     2.236068
    5     0.000000
    6     2.828427
    7     2.236068
    8     2.236068
    9     2.828427
    10    0.000000
    11    2.236068
    12    2.828427
    13    2.236068
    14    2.236068
    15    0.000000
    dtype: float64
    """
    # "push" centroids into opposite corners
    x_dist = (q1.centroid_x - q2.centroid_x).abs() + sides.x
    y_dist = (q1.centroid_y - q2.centroid_y).abs() + sides.y
    dists_pre_sqrt = cast('pd.Series[float]', pd.Series(x_dist**2 + y_dist**2))
    quadrat_diagonal_2 = sides.x**2 + sides.y**2
    dists_pre_sqrt[dists_pre_sqrt == quadrat_diagonal_2] = 0
    dists = cast('pd.Series[float]', np.sqrt(dists_pre_sqrt))
    return dists
