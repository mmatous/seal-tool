import copy
import logging
import random
from collections.abc import Callable, Generator
from functools import singledispatch
from pathlib import Path
from typing import cast

import pandas as pd
from alive_progress.animations.spinners import bouncing_spinner_factory  # type: ignore[import-untyped]
from pandas._typing import Dtype

from seal.config import Config, Sides
from seal.exceptions import InvalidFilterError
from seal.types import Encounters, GridInfo

logger = logging.getLogger(__name__)


def get_spinner() -> Callable[[None], Generator]:  # type: ignore[type-arg]
    emoji = ['🐧', '🐡', '🐟', '🦑', '🦦', '🐠', '🦐', '🐙', '🪼', '🐳']
    random.shuffle(emoji)
    emoji = ''.join(emoji)
    spinner = bouncing_spinner_factory(('🌊', emoji), 6, block=(1, 1), hide=True)
    return cast(Callable[[None], Generator], spinner)  # type: ignore[type-arg]


def analysis_results_filename(out_dir: Path, prefix: str, analysis_info: tuple[str, int]) -> Path:
    if analysis_info[1]:
        return out_dir / f'{prefix}{analysis_info[0]}-{analysis_info[1]}.csv'
    return out_dir / f'{prefix}{analysis_info[0]}.csv'


def enc_dtypes() -> dict[str, Dtype]:
    direction = pd.CategoricalDtype(categories=['b', 'f'])
    phase = pd.CategoricalDtype(categories=['ad', 'init', 'juv', 'sub', 'term'])
    return {
        'significant_size': 'bool',
        'direction': direction,
        'phase': phase,
        'ref': 'Int64',
    }


def qlist_dtypes() -> dict[str, Dtype]:
    q_type = pd.CategoricalDtype(['normal', 'no-reef', 'riptide', 'shallows'])
    return {
        'coord_x': 'Int64',
        'coord_y': 'Int64',
        'quadrat_type': q_type,
    }


def geom_r2(start: float, end: float) -> list[float]:
    res = []
    current = start
    while current <= end:
        res.append(current)
        current *= 2
    return res


def df_levels(df: pd.DataFrame) -> Generator[tuple[pd.DataFrame, int], None, None]:
    levels = df['level'].unique()
    for level in sorted(levels):
        ldf = df[df.level == level]
        yield (ldf, int(level))


def make_grid(cfg: Config) -> GridInfo:
    """Load encounters and quadrat list from files, if necessary and filter them."""
    enc = cfg.encounters if isinstance(cfg.encounters, pd.DataFrame) else load_encounters(cfg.encounters)
    try:
        enc = filter_encounters(enc, cfg)
    except AttributeError as e:
        if "'species'" in str(e):
            raise
        logger.exception('error filtering')
        raise InvalidFilterError(e.name, 'encounters') from e

    quadrats = load_quadrats(cfg.quadrat_list)
    if cfg.quadrat_types and quadrats is not None:
        quadrats = quadrats[quadrats.quadrat_type.isin(cfg.quadrat_types)]
    return GridInfo.new(enc, quadrats)


def load_encounters(dataset: Path) -> Encounters:
    """Load CSV dataset with encounters from `dataset`."""
    dtype_dict = enc_dtypes()
    enc = pd.read_csv(dataset, dtype=dtype_dict).convert_dtypes()
    if 'date' in enc.columns:
        enc['date'] = pd.to_datetime(enc.date, format='ISO8601')
    return enc


def filter_encounters(enc: pd.DataFrame, cfg: Config) -> pd.DataFrame:
    """Return encounters filtered according to criteria specified in `cfg`.

    Supported criteria are documented in example-task.toml.
    """
    if cfg.locality_name:
        enc = enc[enc.locality == cfg.locality_name]
    if cfg.from_:
        enc = enc[enc.date >= cfg.from_]
    if cfg.to:
        enc = enc[enc.date <= cfg.to]

    if cfg.direction == 'richer':
        dirdiff = direction_difference(enc)
        enc = enc.join(dirdiff, on=['coord_x', 'coord_y'], validate='m:1')
        # drop if (forward_has_more_species) and (direction_is_b)
        enc = enc[~((enc['diff_#'] >= 0) & (enc.direction == 'b'))]
        enc = enc[~((enc['diff_#'] < 0) & (enc.direction == 'f'))]
        enc = enc.drop(['f_species', 'b_species', 'diff_%', 'diff_#'], axis='columns', errors='raise')
    elif cfg.direction[0] in {'backward', 'forward'}:
        enc = enc[enc.direction == cfg.direction[0]]

    if cfg.include_families:
        enc = enc[enc.family.isin(cfg.include_families)]
    elif cfg.exclude_families:
        enc = enc[~enc.family.isin(cfg.exclude_families)]
    if cfg.exclude_phases:
        enc = enc[~enc.phase.isin(cfg.exclude_phases)]
    if not cfg.include_tiny:
        enc = enc[enc.significant_size]

    if cfg.use_morph:
        # backup for analyses where orig values are needed for indistinguishables
        # currently only a1
        enc['species_orig'] = enc['species']
        enc['species'] = enc['morph']
    if cfg.discard_indistinguishable:
        indist_mask = enc.species.str.endswith(' sp.', na=True)
        enc = enc[~indist_mask]
    enc = enc.reset_index(drop=True)
    return enc


@singledispatch
def load_quadrats(qlist: pd.DataFrame | Path | None) -> pd.DataFrame | None:
    logger.error('bad load_quadrats called')
    raise TypeError(type(qlist).__name__)


@load_quadrats.register
def _load_quadrats_none(qlist: None) -> None:
    logger.debug('qlist is none')
    return qlist


@load_quadrats.register
def _load_quadrats_path(qlist: Path) -> pd.DataFrame:
    logger.debug('qlist is path')
    return pd.read_csv(qlist, dtype=qlist_dtypes(), index_col=['coord_x', 'coord_y']).convert_dtypes()


@load_quadrats.register
def _load_quadrats_df(qlist: pd.DataFrame) -> pd.DataFrame:
    logger.debug('qlist is df')
    return qlist


def direction_difference(enc: Encounters) -> pd.DataFrame:
    """Return dataframe with absolute and relative differences in encountered species between sampling directions.

    Resulting DataFrame has (coord_x, coord_y) as index and columns 'f_species' and 'b_species'
    describing how many species were sampled in a given direction, including absolute and
    relative differences between the two.

    >>> df = pd.DataFrame(
    ...     {
    ...         'coord_x': ['0', '0', '0', '1', '1'],
    ...         'coord_y': ['0', '0', '0', '1', '1'],
    ...         'direction': ['f', 'f', 'b', 'f', 'b'],
    ...         'species': ['A', 'B', 'A', 'B', 'B'],
    ...     }
    ... )
    >>> direction_difference(df)  # doctest: +NORMALIZE_WHITESPACE
                     f_species  b_species  diff_%  diff_#
    coord_x coord_y
    0       0                2          1     0.5       1
    1       1                1          1     0.0       0
    """
    forward = enc[enc.direction == 'f'][['coord_x', 'coord_y', 'species']]
    backward = enc[enc.direction == 'b'][['coord_x', 'coord_y', 'species']]
    f_lens = (
        forward.groupby(['coord_x', 'coord_y'])
        .nunique()
        .rename({'species': 'f_species'}, axis='columns', errors='raise')
    )
    b_lens = (
        backward.groupby(['coord_x', 'coord_y'])
        .nunique()
        .rename({'species': 'b_species'}, axis='columns', errors='raise')
    )
    stats = f_lens.join(b_lens, how='outer')
    stats['diff_%'] = (stats['f_species'] - stats['b_species']) / stats['f_species']
    stats['diff_#'] = stats['f_species'] - stats['b_species']
    return stats


def transform_grid(gri: GridInfo, cfg: Config) -> tuple[GridInfo, Sides]:
    """Transform grid according to settings in taskfile."""
    sides = cfg.quadrat_sides
    qlist_adj = add_quadrat_centroids(gri.qlist, sides)
    gri_adj = GridInfo(gri.enc, qlist_adj)
    gri_adj, sides_adj = discard_axes(
        gri_adj, sides, discard_transect=cfg.discard_transect_info, discard_zone=cfg.discard_zone_info
    )
    sides_adj = Sides(sides_adj.x, sides_adj.y)
    return (gri_adj, sides_adj)


def discard_axes(gri: GridInfo, sides: Sides, *, discard_transect: bool, discard_zone: bool) -> tuple[GridInfo, Sides]:
    """Transform coordinates in DataFrames to reflect discarding transect and/or zone information if requested.

    >>> enc = pd.DataFrame(
    ...     {
    ...         'coord_x': [0, 1, 1, 1, 1],
    ...         'coord_y': [0, 0, 1, 0, 1],
    ...         'species': ['B', 'A', 'A', 'C', 'B'],
    ...     }
    ... )
    >>> idx = pd.MultiIndex.from_arrays([[0, 0, 1, 1, 2], [0, 1, 0, 1, 0]], names=('coord_x', 'coord_y'))
    >>> quadrats = pd.DataFrame(
    ...     index=idx,
    ...     data={
    ...         'centroid_x': [0.5, 0.5, 1.5, 1.5, 2.5],
    ...         'centroid_y': [0.5, 1.5, 0.5, 1.5, 0.5],
    ...     },
    ... )
    >>> gri = GridInfo(enc, quadrats)
    >>> sides = Sides(1, 1)
    >>> res = discard_axes(gri, sides, discard_transect=True, discard_zone=False)
    >>> res[0].enc
       coord_x  coord_y species
    0        0        0       B
    1        0        0       A
    2        0        1       A
    3        0        0       C
    4        0        1       B
    >>> res[0].qlist  # doctest: +NORMALIZE_WHITESPACE
                     centroid_x  centroid_y
    coord_x coord_y
    0       0               1.5       0.5
            1               1.5       1.5
    >>> res[1]
    Sides(x=3.0, y=1.0)

    >>> res = discard_axes(gri, sides, discard_transect=False, discard_zone=True)
    >>> res[0].enc
       coord_x  coord_y species
    0        0        0       B
    1        1        0       A
    2        1        0       A
    3        1        0       C
    4        1        0       B
    >>> res[0].qlist  # doctest: +NORMALIZE_WHITESPACE
                     centroid_x  centroid_y
    coord_x coord_y
    0       0               0.5         1.0
    1       0               1.5         1.0
    2       0               2.5         1.0
    >>> res[1]
    Sides(x=1.0, y=2.0)


    >>> res = discard_axes(gri, sides, discard_transect=True, discard_zone=True)
    >>> res[0].enc
       coord_x  coord_y species
    0        0        0       B
    1        0        0       A
    2        0        0       A
    3        0        0       C
    4        0        0       B
    >>> res[0].qlist  # doctest: +NORMALIZE_WHITESPACE
                     centroid_x  centroid_y
    coord_x coord_y
    0       0               1.5         1.0
    >>> res[1]
    Sides(x=3.0, y=2.0)


    >>> res = discard_axes(gri, sides, discard_transect=False, discard_zone=False)
    >>> res[0].enc
       coord_x  coord_y species
    0        0        0       B
    1        1        0       A
    2        1        1       A
    3        1        0       C
    4        1        1       B
    >>> res[0].qlist  # doctest: +NORMALIZE_WHITESPACE
                     centroid_x  centroid_y
    coord_x coord_y
    0       0               0.5         0.5
            1               0.5         1.5
    1       0               1.5         0.5
            1               1.5         1.5
    2       0               2.5         0.5
    >>> res[1]
    Sides(x=1.0, y=1.0)
    """
    grid = copy.deepcopy(gri)
    enc, qlist = grid.enc, grid.qlist
    new_x_side, new_y_side = sides.x, sides.y
    qlist = qlist.reset_index()

    if discard_transect:
        new_x_side = sides.x * (qlist.coord_x.max() + 1)
        enc['coord_x'] = 0
        qlist['coord_x'] = 0
    if discard_zone:
        new_y_side = sides.y * (qlist.coord_y.max() + 1)
        enc['coord_y'] = 0
        qlist['coord_y'] = 0

    sides = Sides(new_x_side, new_y_side)
    qlist = qlist.drop_duplicates(['coord_x', 'coord_y'])
    qlist = qlist.set_index(['coord_x', 'coord_y'], verify_integrity=True)
    qlist = add_quadrat_centroids(qlist, sides)
    return GridInfo(enc, qlist), sides


def add_quadrat_centroids(quadrats: pd.DataFrame, sides: Sides) -> pd.DataFrame:
    """Add centroid_x and centroid_y columns to *quadrats* with coordinates of quadrats' centroids.

    >>> qlist = pd.DataFrame(
    ...     index=pd.MultiIndex.from_arrays([[0, 0, 1, 1, 2], [0, 1, 0, 1, 0]], names=('coord_x', 'coord_y'))
    ... )
    >>> add_quadrat_centroids(qlist, Sides(1, 2))  # doctest: +NORMALIZE_WHITESPACE
                     centroid_x  centroid_y
    coord_x coord_y
    0       0               0.5         1.0
            1               0.5         3.0
    1       0               1.5         1.0
            1               1.5         3.0
    2       0               2.5         1.0
    """
    quadrats = copy.deepcopy(quadrats)
    quadrats['centroid_x'] = (quadrats.index.get_level_values('coord_x') * sides.x) + (sides.x / 2)
    quadrats['centroid_y'] = (quadrats.index.get_level_values('coord_y') * sides.y) + (sides.y / 2)
    return quadrats
