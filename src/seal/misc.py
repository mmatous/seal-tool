import argparse
import logging
import math
from typing import NamedTuple, TypedDict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns  # type: ignore[import-untyped] # wontfix'ed https://github.com/mwaskom/seaborn/issues/2212, partial https://github.com/mwaskom/seaborn/issues/3287

from seal.config import Config
from seal.exceptions import UnsuitableGridError
from seal.types import Coord, GridInfo
from seal.util import df_levels, make_grid, qlist_dtypes, transform_grid

logger = logging.getLogger(__name__)


class A2DiffRes(NamedTuple):
    diff: pd.DataFrame
    ref_lvl: int


class Lost(NamedTuple):
    """Represents data lost after grid adjustment."""

    encounters: int
    species: set[str]
    individuals: int | None
    quadrats: set[Coord]


class QlistChecks(TypedDict, total=False):
    dups: 'pd.Index[int]'
    extra_cols: list[str]
    missing_cols: list[str]
    min_x: int
    min_y: int
    n_quadrats: int  # noqa: F841, RUF100  # make vulture + ruff happy
    na_cols: 'pd.Index[int]'  # noqa: F841, RUF100


CZ_KFME_X_MIN = 38
CZ_KFME_Y_MIN = 49


def check_quadrat_list_cli(args: argparse.Namespace) -> None:
    qlist_path = args.quadrat_list
    qlist = pd.read_csv(qlist_path, dtype=qlist_dtypes())

    errs = check_quadrat_list(qlist)
    if 'missing_cols' in errs:
        print(f'Invalid header in {qlist_path}, missing {errs['missing_cols']}')
    if 'dups' in errs:
        print(f'Duplicate coordinates found: {errs['dups']}')
    if 'min_x' in errs:
        print(f'Unusual minimal x coordinate: {errs['min_x']}')
    if 'min_y' in errs:
        print(f'Unusual minimal y coordinate: {errs['min_y']}')
    if 'n_quadrats' in errs:
        max_x, max_y = qlist.coord_x.max(), qlist.coord_y.max()
        print(
            f'Unusual (non-rectangular) number of quadrats, {errs['n_quadrats']} != {(max_x + 1) * (max_y + 1)}, ({max_x + 1} * {max_y + 1})'
        )
    if 'extra_cols' in errs:
        print(f'Possibly redundant column(s): {errs['extra_cols']}')
    if 'na_cols' in errs:
        print(f'Missing or invalid value(s): {errs['na_cols']}')


def check_quadrat_list(qlist: pd.DataFrame) -> QlistChecks:
    """Check quadrat lists for possibly erroneous values.

    :param qlist: quadrat list to check
    :return: dictionary with performed checks as keys and erroneous values corresponding to check
    """
    errs = QlistChecks()

    mandatory_cols = {'coord_x', 'coord_y'}
    qlist_cols = set(qlist.columns)
    missing_cols = mandatory_cols - qlist_cols
    if missing_cols:
        errs['missing_cols'] = sorted(missing_cols)
        return errs

    dups = qlist[['coord_x', 'coord_y']].duplicated()
    if not dups.empty:
        errs['dups'] = qlist.index[dups]

    min_x = qlist.coord_x.min()
    if min_x != 0:
        errs['min_x'] = min_x

    min_y = qlist.coord_y.min()
    if min_y != 0:
        errs['min_y'] = min_y

    max_x, max_y = qlist.coord_x.max(), qlist.coord_y.max()
    quadrats_total = (max_x + 1) * (max_y + 1)
    if len(qlist) != quadrats_total:
        errs['n_quadrats'] = len(qlist)

    processed_cols = {'coord_x', 'coord_y', 'quadrat_type'}
    extra_cols = qlist_cols - processed_cols
    if extra_cols:
        errs['extra_cols'] = sorted(extra_cols)

    nas = qlist.isna().any(axis='columns')
    if nas.any():
        errs['na_cols'] = qlist.index[nas]

    return errs


def join_analyses(args: argparse.Namespace) -> None | str:
    """Concatenate analysis results for joint plotting.

    >>> from io import StringIO
    >>> df0 = pd.DataFrame({'result1': ['0', '1'], 'result2': ['2', '3']}).to_csv(index=False)
    >>> df1 = pd.DataFrame({'result1': ['4', '5'], 'result2': ['6', '7']}).to_csv(index=False)
    >>> inputs = [StringIO(df0), StringIO(df1)]
    >>> args = argparse.Namespace()
    >>> args.inputs, args.output, args.discriminants = inputs, None, None
    >>> pd.read_csv(StringIO(join_analyses(args)))
       result1  result2  discriminant
    0        0        2             0
    1        1        3             0
    2        4        6             1
    3        5        7             1
    """
    df_list = []
    args.discriminants = args.discriminants if args.discriminants else range(len(args.inputs))
    for df_path, discr in zip(args.inputs, args.discriminants, strict=True):
        df = pd.read_csv(df_path)
        df['discriminant'] = discr
        df_list.append(df)
    joined = pd.concat(df_list)
    return joined.to_csv(args.output, index=False)  # type: ignore[no-any-return] # has str type, mypy 1.9.0 bug?


def convert_biolib_cli(args: argparse.Namespace) -> None:
    """Load, convert for analysis and save a biolib dataset."""
    biolib_dtypes: dict[str, str | pd._typing.Dtype] = {
        'SQUARE': str,
        'SUBSQ': pd.CategoricalDtype(categories=['a', 'b', 'c', 'd']),
    }
    biolib = pd.read_csv(args.input, dtype=biolib_dtypes, parse_dates=['CREATED'], date_format='%d/%m/%Y')
    conv = convert_biolib(biolib)
    conv.to_csv(args.output, index=False)


def convert_biolib(biolib: pd.DataFrame) -> pd.DataFrame:
    """Convert dataset from https://www.biolib.cz/cz/speciesmappings for preprocessing and analysis.

    :param biolib: dataframe with columns CREATED, DAY, LATIN, MONTH, SUBSQ, SQUARE, QUANTITY, YEAR
    :returns: dataframe with date, coord_x, coord_y, species, date, and individuals columns
    :raises AttributeError: if one or more of expected columns is missing
    :raises KeyError: if one or more of expected columns is missing
    :raises ValueError: SUBSQ column has values other than `a`, `b`, `c` or `d`.
    """
    biolib = biolib.rename(columns=str.lower).convert_dtypes()
    subsq_cat = pd.CategoricalDtype(categories=['a', 'b', 'c', 'd'])
    biolib['subsq'] = biolib.subsq.astype(subsq_cat)
    if biolib.subsq.isna().any():
        logger.error('ERR: SUBSQ column has value(s) other than one of {a, b, c, d}')
        raise ValueError('subsq')
    biolib['created'] = pd.to_datetime(biolib.created, format='%d/%m/%Y')
    biolib = biolib.rename(columns={'latin': 'species', 'quantity': 'individuals'}, errors='raise')
    biolib['individuals'] = pd.to_numeric(biolib['individuals'], errors='coerce').fillna(1)

    # workaround because some sighting dates are invalid (day 0 or month 0)
    biolib['month'] = np.where(biolib.month == 0, biolib.created.dt.month, biolib.month)
    biolib['day'] = np.where(biolib.day == 0, biolib.created.dt.day, biolib.day)
    dateframe = biolib[['year', 'month', 'day']]
    biolib['date'] = pd.to_datetime(dateframe)

    coord_x = biolib['square'].str[2:]
    biolib['coord_x'] = coord_x.astype('Int64')
    biolib['coord_x'] = biolib['coord_x'] - CZ_KFME_X_MIN

    coord_y = biolib['square'].str[:2]
    biolib['coord_y'] = coord_y.astype('Int64')
    biolib['coord_y'] = biolib['coord_y'] - CZ_KFME_Y_MIN

    # adjust for subsquares for more fine-grained grid
    # subsquare layout:
    #       Y low
    # X low  a b  X high
    # X low  c d  X high
    #       Y high
    biolib['coord_x'] *= 2
    is_lower_x = (biolib.subsq == 'a') | (biolib.subsq == 'c')
    biolib['coord_x'] = biolib['coord_x'].where(is_lower_x, biolib.coord_x + 1)

    biolib['coord_y'] *= 2
    is_lower_y = (biolib.subsq == 'a') | (biolib.subsq == 'b')
    biolib['coord_y'] = biolib['coord_y'].where(is_lower_y, biolib.coord_y + 1)

    for axis in ('coord_x', 'coord_y'):
        biolib[axis] -= biolib[axis].min()

    biolib = biolib[['coord_x', 'coord_y', 'species', 'date', 'individuals']]
    return biolib.sort_values(by=['coord_x', 'coord_y'])


def convert_aopk_cli(args: argparse.Namespace) -> None:
    """Load, convert for analysis and save an AOPK dataset.

    Input is expected to use CP1250 encoding. This is the default
    after downloading from the source.
    """
    aopk_dtypes = {'SITMAP': str}
    try:
        aopk = pd.read_csv(
            args.input,
            dtype=aopk_dtypes,
            parse_dates=['DATI_INSERT'],
            date_format='%Y%m%d',
            encoding='cp1250',
            delimiter=';',
        )
    except UnicodeDecodeError:
        print('Unusual encoding for AOPK data, CP1250 expected. Trying UTF-8.')
        aopk = pd.read_csv(
            args.input, dtype=aopk_dtypes, parse_dates=['DATI_INSERT'], date_format='%Y%m%d', delimiter=';'
        )
    conv = convert_aopk(aopk)
    conv.to_csv(args.output, index=False, encoding='utf-8')


def convert_aopk(aopk: pd.DataFrame) -> pd.DataFrame:
    """Convert dataset from https://portal.nature.cz/nd/ for preprocessing and analysis.

    :param aopk: dataframe with columns DRUH, DATI_INSERT, SITMAP, POCET
    :returns: dataframe with coord_x, coord_y, species, date, and individuals columns
    :raises AttributeError: if one or more of expected columns is missing
    :raises KeyError: if one or more of expected columns is missing
    """
    aopk = aopk.rename(columns=str.lower).convert_dtypes()
    aopk = aopk.rename(
        columns={
            'dati_insert': 'date',
            'druh': 'species',
            'pocet': 'individuals',
        },
        errors='raise',
    )
    aopk['individuals'] = pd.to_numeric(aopk['individuals'], errors='coerce').fillna(1)
    aopk['date'] = pd.to_datetime(aopk.date, format='%Y%m%d')

    coord_x = aopk['sitmap'].str[2:]
    aopk['coord_x'] = coord_x.astype('Int64')
    aopk['coord_x'] = aopk['coord_x'] - CZ_KFME_X_MIN

    coord_y = aopk['sitmap'].str[:2]
    aopk['coord_y'] = coord_y.astype('Int64')
    aopk['coord_y'] = aopk['coord_y'] - CZ_KFME_Y_MIN

    for axis in ('coord_x', 'coord_y'):
        aopk[axis] -= aopk[axis].min()

    return aopk[['coord_x', 'coord_y', 'species', 'date', 'individuals']]


def a2_difference_cli(args: argparse.Namespace) -> None:
    """Present `sar_lvl_diff()` results in human-readable way."""
    a2_res = pd.read_csv(args.a2_results_aux)
    diffs, ref_lvl = sar_lvl_diff(a2_res, is_rtm=args.rtm)

    print(f'Total sampled species: {int(a2_res.n_species_mean.max())}')
    print('% difference:')
    for lvl_df, lvl in df_levels(diffs):
        pct = lvl_df['pct_diff']
        print(f'\nLevel {lvl} vs {ref_lvl}:')
        print(pct.describe().drop('count').round(1).to_string())
        print(f'MAPE:\t{np.mean(np.abs(pct)).round(1)}')

    sns.set_theme(palette='colorblind', rc={'backend': 'svg'})
    fig, ax = plt.subplots()
    sns.lineplot(x='area', y='diff', hue='level', ax=ax, data=diffs)
    ax.set(title='Level differences', xlabel=r'Area ($m^2$)', ylabel='Spp. difference')
    fig.savefig(args.a2_results_aux.with_suffix('.svg'))


def sar_lvl_diff(a2_aux_data: pd.DataFrame, *, is_rtm: bool = False) -> A2DiffRes:
    """Quantify spp. difference between levels based on results from species-area relationship analysis.

    :param a2_aux_data: the auxiliary data from species-area relationship analysis (a2)
    :param is_rtm: `True` to indicate that `a2_aux_data` was generated using repeated transect merging strategy
    :returns: NamedTuple with `diffs` and `ref_lvl` attributes.
        `diffs` is a DataFrame with the following columns: `diff`, `area`, `pct_diff`, `level`
        `ref_lvl` denotes what level from the passed DataFrame was chosen as a reference
    :raises KeyError: if level column is missing
    :raises AttributeError: if any other column is missing
    :seealso: `a2_sar()`
    """
    ref_lvl = a2_aux_data['level'].min() if is_rtm else a2_aux_data['level'].max()
    other_levels = a2_aux_data[a2_aux_data.level != ref_lvl]
    ref_grid = a2_aux_data[a2_aux_data.level == ref_lvl]

    diffs = []
    for lvl_grid, level in df_levels(other_levels):
        lbound = max(ref_grid.area.min(), lvl_grid.area.min())
        rbound = min(ref_grid.area.max(), lvl_grid.area.max())
        xs = pd.concat([ref_grid.area, lvl_grid.area], ignore_index=True)
        clip_xs = np.unique(np.clip(xs, lbound, rbound))
        ref_ys = np.interp(clip_xs, ref_grid.area, ref_grid.n_species_mean)
        lvl_ys = np.interp(clip_xs, lvl_grid.area, lvl_grid.n_species_mean)

        diffs.append(
            {
                'diff': lvl_ys - ref_ys,
                'area': clip_xs,
                # ref_ys being a mean won't be 0, unless dealing with empty grid
                'pct_diff': pd.Series(100 * (lvl_ys - ref_ys) / ref_ys),
                'level': level,
            }
        )
    res = pd.concat([pd.DataFrame(df) for df in diffs])
    return A2DiffRes(res, ref_lvl)


def adjust_grid_cli(args: argparse.Namespace) -> None:
    """Call `adjust_grid()` and present the results on the command line."""
    cfg = Config.from_taskfile(args.taskfile)
    orig_grid = make_grid(cfg)
    (tgrid, sides_adj) = transform_grid(orig_grid, cfg)
    adj_grid, lost = adjust_grid(cfg, tgrid)
    n_quad = len(lost.quadrats)
    q_area = sides_adj.x * sides_adj.y
    print(
        f'Old dimension: {tgrid.qlist.index.get_level_values('coord_x').max() + 1}×{tgrid.qlist.index.get_level_values('coord_y').max() + 1}. '  # noqa: RUF001
        f'New dimensions: {adj_grid.qlist.index.get_level_values('coord_x').max() + 1}×{adj_grid.qlist.index.get_level_values('coord_y').max() + 1}.\n'  # noqa: RUF001
        f'Lost:\n\t{lost.encounters}/{len(tgrid.enc)} encounters\n'
        f'\t{len(lost.species)}/{tgrid.enc.species.nunique()} species: {", ".join(sorted(lost.species))}\n'
        f'\t{n_quad * q_area}/{len(tgrid.qlist) * q_area} area units\n'
        f'\t{len(lost.quadrats)}/{len(tgrid.qlist)} quadrats: {", ".join(map(str, sorted(lost.quadrats)))}'
    )
    strat_abbr = ''.join([substr[0] for substr in cfg.level_strategy.split('-')])
    parent = args.output if args.output else cfg.encounters.parent
    adj_grid.enc.to_csv(parent / f'{cfg.encounters.stem}-{strat_abbr}.csv', index=False)

    if cfg.quadrat_list:
        adj_qlist = adj_grid.qlist.filter(['quadrat_type'])
        parent = args.output if args.output else cfg.quadrat_list.parent
        adj_qlist.to_csv(parent / f'{cfg.quadrat_list.stem}-{strat_abbr}.csv')


def get_metrics(gi: GridInfo) -> tuple[int, int, int]:
    """Return grid metrics relevant for comparison between adjusted and original grid.

    >>> get_metrics(GridInfo(pd.DataFrame({'species': ['a', 'b', 'b', 'c']}), pd.DataFrame({'q': [1, 2]})))
    (4, 3, 2)
    """
    return (len(gi.enc), gi.enc.species.nunique(), len(gi.qlist))


def get_lost(grid: GridInfo, adj_grid: GridInfo) -> Lost:
    """Return statistics for information lost while adjusting study grid for analysis."""
    lost_individuals = None
    if 'individuals' in grid.enc.columns:
        lost_individuals = grid.enc.individuals.sum() - adj_grid.enc.individuals.sum()
    orig_coords = set(zip(grid.qlist.coord_x.tolist(), grid.qlist.coord_y.tolist(), strict=True))
    adj_coords = set(zip(adj_grid.qlist.coord_x.tolist(), adj_grid.qlist.coord_y.tolist(), strict=True))
    lost_quadrats = orig_coords - adj_coords
    lost_species = set(grid.enc.species.unique()) - set(adj_grid.enc.species.unique())
    return Lost(len(grid.enc) - len(adj_grid.enc), lost_species, lost_individuals, lost_quadrats)


def adjust_grid(cfg: Config, grid: GridInfo) -> tuple[GridInfo, Lost]:
    """Adjust grid in a way that loses minimum information."""
    grid.qlist['coord_x'] = grid.qlist.index.get_level_values('coord_x')
    grid.qlist['coord_y'] = grid.qlist.index.get_level_values('coord_y')
    x_dim = grid.qlist.coord_x.max() + 1
    y_dim = grid.qlist.coord_y.max() + 1
    max_lvl = max(cfg.levels)
    if cfg.level_strategy == 'nested-quadrats':
        merged_q_dim = 2 ** (max_lvl - 1)
        x_cutoff, y_cutoff = (mx % merged_q_dim for mx in [x_dim, y_dim])
        if merged_q_dim > x_dim - x_cutoff:
            problem_levels = [lvl for lvl in cfg.levels if 2 ** (lvl - 1) > x_dim]
            raise UnsuitableGridError(cfg.level_strategy, problem_levels, 'x', 'adjustment impossible, axis too small')
        if merged_q_dim > y_dim - y_cutoff:
            problem_levels = [lvl for lvl in cfg.levels if 2 ** (lvl - 1) > y_dim]
            raise UnsuitableGridError(cfg.level_strategy, problem_levels, 'y', 'adjustment impossible, axis too small')

        left_part = 'coord_x < @x_dim - @x_cutoff'
        bottom_part = 'coord_y < @y_dim - @y_cutoff'
        right_part = 'coord_x >= @x_cutoff'
        top_part = 'coord_y >= @y_cutoff'
        options = [
            f'{bottom_part} & {left_part}',
            f'{bottom_part} & {right_part}',
            f'{top_part} & {left_part}',
            f'{top_part} & {right_part}',
        ]
    elif cfg.level_strategy == 'striped-transect-merging':
        x_cutoff = x_dim % max_lvl
        if max_lvl > x_dim - x_cutoff:
            problem_levels = [lvl for lvl in cfg.levels if lvl > x_dim]
            raise UnsuitableGridError(cfg.level_strategy, problem_levels, 'x', 'adjustment impossible, axis too small')
        options = ['coord_x < @x_dim - @x_cutoff', 'coord_x >= @x_cutoff']
    elif cfg.level_strategy == 'repeated-transect-merging':
        lcm = math.lcm(*cfg.levels)
        x_cutoff = x_dim % lcm
        if lcm > x_dim:
            problem_levels = [lvl for lvl in cfg.levels if (x_dim % lvl != 0)]
            raise UnsuitableGridError(cfg.level_strategy, problem_levels, 'x', 'adjustment impossible, axis too small')
        options = ['coord_x < @x_dim - @x_cutoff', 'coord_x >= @x_cutoff']
    else:
        raise ValueError(cfg.level_strategy)

    # engine='python' -> https://github.com/pandas-dev/pandas/issues/25369
    gr_infos = [
        GridInfo(grid.enc.query(opt, engine='python'), grid.qlist.query(opt, engine='python')) for opt in options
    ]
    metrics = [get_metrics(gri) for gri in gr_infos]

    best_idx = metrics.index(max(metrics))
    adj_grid = gr_infos[best_idx]
    lost = get_lost(grid, adj_grid)
    for axis in ['coord_x', 'coord_y']:
        adj_grid.enc.loc[:, axis] -= adj_grid.enc[axis].min()
        adj_grid.qlist.loc[:, axis] -= adj_grid.qlist[axis].min()
    adj_grid = GridInfo(adj_grid.enc, adj_grid.qlist.set_index(['coord_x', 'coord_y'], verify_integrity=True))
    return (adj_grid, lost)
