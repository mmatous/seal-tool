"""Shared types."""

import dataclasses
from typing import Literal, NamedTuple, Self

from pandas import DataFrame, MultiIndex, Series

type A1Aux = dict[tuple[int, str], 'DataFrame | Series[float]']
type Auxiliary = DataFrame | A1Aux | None
type Axis = Literal['x', 'y']
type Coord = tuple[int, int]
type Distance = Literal['euclid-centroid', 'euclid-diagonal', 'chebyshev']
type Encounters = DataFrame
"""DataFrame with at least coord_x, coord_y and species columns."""
type ErrorStyle = Literal['band', 'bars']
type ErrorType = Literal['ci', 'pi', 'se', 'sd']
type Format = Literal['png', 'svg']
type InputType = Literal['encounters', 'qlist']
type LevelStrategy = Literal[
    'nested-quadrats',
    'repeated-transect-merging',
    'striped-transect-merging',
]
type PreprocessCheck = Literal[
    'dups',
    'individuals',
    'family',
    'morph-species',
    'nas',
    'quartets',
    'refs',
    'strs',
    'species-name',
    'species-phase-morph',
]
type QList = DataFrame
"""DataFrame with at coord_x, coord_y as MultiIndex."""
type QuadratType = Literal['normal', 'no-reef', 'riptide', 'shallows']
type Result = DataFrame
type SamplingDirection = Literal['any', 'backward', 'forward', 'richer']


class AnalysisResult(NamedTuple):
    res: Result
    """DataFrame with the main result of analysis"""
    aux: Auxiliary
    """Optional auxiliary results of analysis"""


type AnalysisResults = dict[tuple[str, int], AnalysisResult]
"""A dictionary with results of analyses.

    - keys: (`analysis_type`, `analysis_position`) where `analysis_position` serves as additional identifier
        in case multiple analyses of the same type are requested in the config.
    - values: a namedtuple of (main_results, auxiliary data), usually dataframes but auxiliary data may be None
        or string for some analyses
"""


@dataclasses.dataclass(frozen=True, slots=True)
class GridInfo:
    """Contains information about spp. encounters and layout of the study grid."""

    enc: Encounters
    qlist: QList

    @classmethod
    def new(cls, enc: Encounters, qlist: QList | None) -> Self:
        if qlist is None:
            idx = MultiIndex.from_frame(enc[['coord_x', 'coord_y']].drop_duplicates())
            qlist = DataFrame(index=idx)
        enc = enc.set_flags(allows_duplicate_labels=False)
        qlist = qlist.set_flags(allows_duplicate_labels=False)
        qlist = qlist.sort_index()
        return cls(enc, qlist)
