# Changelog

## [Unreleased]

### Changed

- **Breaking:** check grid shape suitability for chosen strategy and level selection. Unsuitable grids will be rejected to prevent distorted results.
- **Breaking:** new a4 implementation that's better in line with the original one from Palmer & White.
- **Breaking:** new a5 implementation that's better in line with the original one from Palmer & White. Previous one resulted only in single ratio for the entire grid for any given level.
- **Breaking:** force-merge quadrats in the same transect for transect-centric strategies. This is to prevent behaviour where in some analyses
extent grew significantly in one direction while stagnating in
the other.

- **Breaking:** rename distances from diagonal, centroid and max1d to euclid-diagonal, euclid-centroid and chebyshev respectively
- **Breaking:** remove taskfile table [locality]. Keys that it previously contained were moved into top-level table.
This both simplifies API for analysis config creation and reduces room for error in taskfiles (putting keys under
wrong table) for people new to TOML.
In addition `name` and `sides` keys formerly under `[locality]` were renamed to `locality_name` and `quadrat_sides`
respectively.
- **Breaking:** shift AOPK and Biolib coordinates to (0, 0) during conversion.
- Rename secondary analysis results from additional data to auxiliary data.

### Added

- Add union and difference of encountered species for quadrat pairs in analysis a3.
- Add subcommand to quantify and plot auxiliary data results from species-area relationship analysis (a2).
`seal misc adjust-grid` subcommand was added to help adjust grid for suitability criteria conformance.
- Create an API to enable use as a library (analysis, preprocess and misc)

### Removed

- **Breaking:** remove all strategies except `nested-quadrats`, `repeated-transect-merging`
and `striped transect merging`. They were pretty much just experiments and not really useful or plainly wrong.
- **Breaking:** remove estimation of expected number of species from a8. Recent literature shows previously used
Preston's formula is based on [wrong assumptions](https://www.sci.muni.cz/botany/nekola/nekola%20pdf/fg-43-259-268.pdf), such as
employing bins of exponentially increasing size resulting in hump-shaped pattern that is not congruent with the shape of untransformed distribution, and should not be used.
- **Breaking:** remove subcommands from misc and checks from preprocess that required `quadrat_id` column
with overly specific format.
- Remove Jupyter notebook for lack of interest. Target audience indicates strong preference for R interface instead.


### Fixed

- Fix windows compatibility by enforcing utf-8 encoding.
- Fix binning in a8 to include number of individuals equal to the rightmost interval boundary.
- Display description and help string whitespaces correctly.
- Fix per-quadrat stats computation in a1 auxiliary data.
- Prevent iterative division of coordinates in RTM strategy, which resulted
in coordinates having lower values than they should in higher levels.
- Fix incorrect quadrat side and coordinate recalculation when discarding transects or zones
- Fix centroid and quadrat adjustment for nested-quadrats strategy.

## [0.11.0] - 2024-04-26

### Changed

- **Breaking:** change taskfile format to TOML
- **Breaking:** make `quadrat_type` column in quadrat lists optional. The default behavior of quadrat_types parameter in taskfile is now not to filter anything. Use `quadrat_type = ["normal"]` to retain old behavior.
- Do not make use of `quadrat_id` column in input data thus making it optional
- Make optional every column except `species`, `coord_x` and `coord_y`
- Make quadrat list optional
- Always shift AOPK and Biolib coordinates by the same offset
- Don't include dummy columns while converting data
- Display medians as numeric value in analysis 5 plots
- Display points in analysis 5 with light border (smaller than outliers)
- Add more spinner emoji
- Make colormap size to be more in line with size of heatmap in a1
- Improve post-level adjustment error reporting
- Improve ticks spacing in a2 SAR graph
- Improve quadrat list type-checking

### Added

- Add barplots depicting (species+individuals)/quadrat for a1
- Cover equitability of abundance, species abundance distribution and theoretical abundance data by newly added a8
- Select graphs format using `output_format` in taskfile
- Add quadrat list example for CZ KFME grid usable with AOPK or Biolib data. This is a complete grid, unlike the previously removed example.

### Removed

- **Breaking:** remove `--png` CLI option. Use `output_format` in taskfile.
- Delete unnecessary AOPK quadrat list example as quadrat lists were made optional.

### Fixed

- Properly seed RNG for reproducible plots
- Biolib conversion now converts x coordinates in proper order. They were switched pair-wise due to a bug. E.g. 0<->1, 2<->3...
- Don't overwrite analysis results when computing multiple analysis of the same type (with possibly different analysis parameters)
- Actually use `error-type` config key

## [0.10.0] - 2024-02-07

### Added

- Initial release
