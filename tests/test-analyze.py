import pandas as pd
import pytest

from seal.__main__ import main
from seal.analyze import a4_extent_total, a5_observed_expected, adjust_nested_quadrats
from seal.config import Sides
from seal.exceptions import UnsuitableGridError
from seal.util import add_quadrat_centroids
from tests.util import (
    _observed_path,  # noqa: F401
    compare_frames,
    compare_text,
)


def dmat_idx(sm: pd.DataFrame) -> pd.MultiIndex:
    """Construct MultiIndex from given species matrix."""
    sm = sm.reset_index()[['coord_x', 'coord_y']]
    idx = sm.join(sm, how='cross', rsuffix='_other')
    return pd.MultiIndex.from_frame(idx)


class TestAdjustNestedQuadrats:
    def test_adjust_nested_quadrats_4x4_lvl_1_2(self):
        # XXXO     ----
        # XOXX --> X-X-
        # XOXX --> ----
        # OOXX     O-X-
        enc = pd.DataFrame(
            {
                'coord_x': [0, 1, 1, 1, 3],
                'coord_y': [0, 0, 1, 2, 3],
                'species': ['B', 'A', 'A', 'C', 'B'],
            }
        )
        enc_expected = pd.DataFrame({'coord_x': [0], 'coord_y': [0], 'species': ['B']})
        qlist = pd.DataFrame(
            index=pd.MultiIndex.from_tuples(
                [(0, 0), (0, 1), (1, 0), (1, 1), (1, 2), (3, 3)], names=('coord_x', 'coord_y')
            ),
            data={'centroid_x': [2, 2, 6, 6, 10, 14], 'centroid_y': [2, 6, 2, 6, 10, 14]},
        )
        qlist_expected = pd.DataFrame(
            {'coord_x': [0], 'coord_y': [0], 'centroid_x': [2.0], 'centroid_y': [2.0]}
        ).set_index(['coord_x', 'coord_y'])

        grid, sides = adjust_nested_quadrats(enc, qlist, Sides(4, 4), level=1, max_level=2)

        pd.testing.assert_frame_equal(grid.enc, enc_expected)
        pd.testing.assert_frame_equal(grid.qlist, qlist_expected)
        assert sides == Sides(x=4, y=4)

    def test_adjust_nested_quadrats_8x8_lvl_1_3(self):
        # OOOOOOOO     --------
        # OOOOOOOO     --------
        # OOOOOOOO     --------
        # OOOOOOOO --> O---O---
        # OOOOOOOO --> --------
        # OOOOOOOO     --------
        # OOOOOOOO     --------
        # OOOOOOOO     O---O---
        orig_sides = Sides(x=2, y=2)
        enc = pd.DataFrame({'coord_x': [0], 'coord_y': [0], 'species': ['A']})
        enc_expected = pd.DataFrame({'coord_x': [0], 'coord_y': [0], 'species': ['A']})
        qlist = pd.DataFrame(index=pd.MultiIndex.from_product([range(8), range(8)], names=('coord_x', 'coord_y')))
        qlist = add_quadrat_centroids(qlist, orig_sides)
        centroid_xs = [1.0] * 2 + [9.0] * 2
        centroid_ys = [1.0, 9.0] * 2
        qlist_expected = pd.DataFrame(
            index=pd.MultiIndex.from_product([range(2), range(2)], names=('coord_x', 'coord_y')),
            data={'centroid_x': centroid_xs, 'centroid_y': centroid_ys},
        )

        grid, sides = adjust_nested_quadrats(enc, qlist, orig_sides, level=1, max_level=3)

        pd.testing.assert_frame_equal(grid.enc, enc_expected)
        pd.testing.assert_frame_equal(grid.qlist, qlist_expected)
        assert sides == orig_sides

    def test_adjust_nested_quadrats_8x8_lvl_2_3_regular(self):
        # OOOOOOOO     --------
        # OOOOOOOO     --------
        # OOOOOOOO     OO--OO--
        # OOOOOOOO --> OO--OO--
        # OOOOOOOO --> --------
        # OOOOOOOO     --------
        # OOOOOOOO     OO--OO--
        # OOOOOOOO     OO--OO--
        orig_sides = Sides(x=2, y=2)
        enc = pd.DataFrame({'coord_x': [0], 'coord_y': [0], 'species': ['A']})
        enc_expected = pd.DataFrame({'coord_x': [0], 'coord_y': [0], 'species': ['A']})
        qlist = pd.DataFrame(index=pd.MultiIndex.from_product([range(8), range(8)], names=('coord_x', 'coord_y')))
        qlist = add_quadrat_centroids(qlist, orig_sides)
        centroid_xs = [2.0] * 2 + [10.0] * 2
        centroid_ys = [2.0, 10.0] * 2
        qlist_expected = pd.DataFrame(
            index=pd.MultiIndex.from_product([range(2), range(2)], names=('coord_x', 'coord_y')),
            data={'centroid_x': centroid_xs, 'centroid_y': centroid_ys},
        )

        grid, sides = adjust_nested_quadrats(enc, qlist, orig_sides, level=2, max_level=3)

        pd.testing.assert_frame_equal(grid.enc, enc_expected)
        pd.testing.assert_frame_equal(grid.qlist, qlist_expected)
        assert sides == Sides(x=4, y=4)

    def test_adjust_nested_quadrats_8x8_lvl_2_3_holes(self):
        # OOOOOOOO     --------
        # OOOOOOOO     --------
        # OOOOXXOO     OO--XX--
        # XOOOXXOO --> OO--XX--
        # OOOOOOOO --> --------
        # OOOOOOOO     --------
        # OXOOOOOO     OO--OO--
        # OOOOOOOO     OO--OO--
        orig_sides = Sides(x=1, y=1)
        enc = pd.DataFrame({'coord_x': [0], 'coord_y': [0], 'species': ['A']})
        enc_expected = pd.DataFrame({'coord_x': [0], 'coord_y': [0], 'species': ['A']})
        qlist = pd.DataFrame(index=pd.MultiIndex.from_product([range(8), range(8)], names=('coord_x', 'coord_y'))).drop(
            [(0, 4), (1, 1), (4, 4), (4, 5), (5, 4), (5, 5)]
        )
        qlist = add_quadrat_centroids(qlist, orig_sides)
        qlist_expected = pd.DataFrame(
            index=pd.MultiIndex.from_tuples([(0, 0), (0, 1), (1, 0)], names=('coord_x', 'coord_y')),
            data={'centroid_x': [1.0, 1.0, 5.0], 'centroid_y': [1.0, 5.0, 1.0]},
        )

        grid, sides = adjust_nested_quadrats(enc, qlist, orig_sides, level=2, max_level=3)

        pd.testing.assert_frame_equal(grid.enc, enc_expected)
        pd.testing.assert_frame_equal(grid.qlist, qlist_expected)
        assert sides == Sides(x=2, y=2)


class TestA4:
    def test_1x1_is_empty(self):
        idx = pd.MultiIndex.from_tuples([(0, 0)], names=('coord_x', 'coord_y'))
        spp_matrix = pd.DataFrame({'A': [True], 'B': [True]}, index=idx)
        dmat = pd.DataFrame(data={'distance': [0]}, index=dmat_idx(spp_matrix))
        expected_res = pd.DataFrame({'spp_total': [], 'extent': [], 'spp_mean': []})

        res, add = a4_extent_total(spp_matrix, dmat)

        pd.testing.assert_frame_equal(res, expected_res)
        assert add.empty

    def test_rectangle_minimal_2x2(self):
        idx = pd.MultiIndex.from_product([[0, 1], [0, 1]], names=('coord_x', 'coord_y'))
        spp_matrix = pd.DataFrame({'A': [True] * 4}, index=idx)
        dmat = pd.DataFrame(
            data={'distance': [0, 1, 1, 1.14142,
                               1, 0, 1.14142, 1,
                               1, 1.14142, 0, 1,
                               1.14142, 1, 1, 0],}, index=dmat_idx(spp_matrix)
        )  # fmt: skip
        expected = pd.DataFrame({'spp_total': [1], 'extent': [1.14142], 'spp_mean': [0.25]})

        res, _ = a4_extent_total(spp_matrix, dmat)

        pd.testing.assert_frame_equal(res, expected)

    def test_rectangle_l_shape_is_empty(self):
        idx = pd.MultiIndex.from_product([[0, 1], [0, 1]], names=('coord_x', 'coord_y')).drop((1, 1))
        spp_matrix = pd.DataFrame({'A': [True] * 3}, index=idx)
        dmat = pd.DataFrame(data={'distance': [0]}, index=dmat_idx(spp_matrix))
        expected = pd.DataFrame({'spp_total': [], 'extent': [], 'spp_mean': []})

        res, add = a4_extent_total(spp_matrix, dmat)

        pd.testing.assert_frame_equal(res, expected)
        assert add.empty

    def test_linear_minimal_1x2(self):
        idx = pd.MultiIndex.from_tuples([(0, 0), (0, 1)], names=('coord_x', 'coord_y'))
        spp_matrix = pd.DataFrame({'A': [True, False], 'B': [False, True]}, index=idx)
        dmat = pd.DataFrame(data={'distance': [0, 42, 42, 0]}, index=dmat_idx(spp_matrix))
        expected = pd.DataFrame({'spp_total': [2], 'extent': [42], 'spp_mean': [1.0]})

        res, _ = a4_extent_total(spp_matrix, dmat)

        pd.testing.assert_frame_equal(res, expected)

    def test_linear_3x1(self):
        idx = pd.MultiIndex.from_tuples([(0, 0), (0, 1), (0, 2)], names=('coord_x', 'coord_y'))
        spp_matrix = pd.DataFrame({'A': [True, False, True], 'B': [False, True, False]}, index=idx)
        dmat = pd.DataFrame(data={'distance': [0, 1, 2,  1, 0, 1,  2, 1, 0]}, index=dmat_idx(spp_matrix))  # fmt: skip
        expected = pd.DataFrame({'spp_total': [2, 1, 2], 'extent': [1, 2, 1], 'spp_mean': [1.0, 0.5, 1.0]})

        res, _ = a4_extent_total(spp_matrix, dmat)

        pd.testing.assert_frame_equal(res, expected)


class TestA5:
    def test_a5_rejects_y_lt_4_nonlinear(self):
        coords = [(x, y) for x in range(4) for y in range(3)]
        idx = pd.MultiIndex.from_tuples(coords, names=['coord_x', 'coord_y'])
        spp_matrix = pd.DataFrame(index=idx)
        with pytest.raises(UnsuitableGridError) as einfo:
            a5_observed_expected(spp_matrix)

        assert einfo.value.levels is None
        assert einfo.value.operation == 'a5'
        assert einfo.value.axis == 'y'
        assert einfo.value.add_info == 'grid must be at least 4 rows high'

    def test_a5_rejects_x_lt_3_linear(self):
        idx = pd.MultiIndex.from_tuples([(0, 0), (1, 0), (2, 0)], names=['coord_x', 'coord_y'])
        spp_matrix = pd.DataFrame(index=idx)
        with pytest.raises(UnsuitableGridError) as einfo:
            a5_observed_expected(spp_matrix)

        assert einfo.value.levels is None
        assert einfo.value.operation == 'a5'
        assert einfo.value.axis == 'x'
        assert einfo.value.add_info == 'grid must be at least 4 columns wide'

    def test_a5_minimal_4x4(self):
        coords = [(x, y) for x in range(4) for y in range(4)]
        idx = pd.MultiIndex.from_tuples(coords, names=['coord_x', 'coord_y'])
        spp_matrix = pd.DataFrame(
            {'A': [True] * 4 + [False] * 8 + [True] + [False] * 3, 'B': [False] * 4 + [True] * 12}, index=idx
        )
        expected = pd.DataFrame({'variance_ratio': [0.145631068], 'gap': [0]})

        res, _ = a5_observed_expected(spp_matrix)

        pd.testing.assert_frame_equal(res, expected)

    def test_a5_minimal_4x1(self):
        idx = pd.MultiIndex.from_tuples([(0, 0), (1, 0), (2, 0), (3, 0)], names=['coord_x', 'coord_y'])
        spp_matrix = pd.DataFrame({'A': [True, False, False, True], 'B': [False, True, True, True]}, index=idx)
        expected = pd.DataFrame({'variance_ratio': [0.428571429], 'gap': [0]})

        res, _ = a5_observed_expected(spp_matrix)

        pd.testing.assert_frame_equal(res, expected)

    def test_a5_4x4_gap_is_empty(self):
        coords = [(x, y) for x in range(4) for y in range(4)]
        idx = pd.MultiIndex.from_tuples(coords, names=['coord_x', 'coord_y']).drop((1, 2))
        spp_matrix = pd.DataFrame(
            {'A': [True] * 4 + [False] * 7 + [True] + [False] * 3, 'B': [False] * 4 + [True] * 11}, index=idx
        )

        res, _ = a5_observed_expected(spp_matrix)

        assert res.empty

    # e2e
    def test_a5_rtm(self):
        cl = ['seal', 'analyze', '--taskfile', './tests/tasks/a5-rtm-chebyshev.toml']
        main(cl)
        compare_frames('a5-rtm-chebyshev-a5.csv')

    def test_a5_stm(self):
        cl = ['seal', 'analyze', '--taskfile', './tests/tasks/a5-stm-chebyshev.toml']
        main(cl)
        compare_frames('a5-stm-chebyshev-a5.csv')

    def test_a5_nq(self):
        cl = ['seal', 'analyze', '--taskfile', './tests/tasks/a5-nq-chebyshev.toml']
        main(cl)
        compare_frames('a5-nq-chebyshev-a5.csv')


class TestBasicRTMChebyshev:
    @pytest.fixture(scope='class', autouse=True)
    def _analyze(self) -> None:
        cl = ['seal', 'analyze', '--taskfile', './tests/tasks/basic-rtm-chebyshev.toml']
        main(cl)

    def test_a1(self) -> None:
        compare_frames('basic-rtm-chebyshev-a1.csv')

    def test_a1_aux(self) -> None:
        compare_text('basic-rtm-chebyshev-a1-aux.txt')

    def test_a2(self) -> None:
        compare_frames('basic-rtm-chebyshev-a2.csv')

    def test_a2_aux(self) -> None:
        compare_frames('basic-rtm-chebyshev-a2-aux.csv')

    def test_a3(self) -> None:
        compare_frames('basic-rtm-chebyshev-a3.csv')

    def test_a3_aux(self) -> None:
        compare_frames('basic-rtm-chebyshev-a3-aux.csv')

    def test_a4(self) -> None:
        compare_frames('basic-rtm-chebyshev-a4.csv')

    def test_a4_aux(self) -> None:
        compare_frames('basic-rtm-chebyshev-a4-aux.csv')

    # a5 tested separately to grid shape requirements

    def test_a6(self) -> None:
        compare_frames('basic-rtm-chebyshev-a6.csv')

    def test_a6_aux(self) -> None:
        compare_frames('basic-rtm-chebyshev-a6-aux.csv')

    def test_a7(self) -> None:
        compare_frames('basic-rtm-chebyshev-a7.csv')

    def test_a8(self) -> None:
        compare_frames('basic-rtm-chebyshev-a8.csv')


class TestBasicSTMChebyshev:
    @pytest.fixture(scope='class', autouse=True)
    def _analyze(self) -> None:
        cl = ['seal', 'analyze', '--taskfile', './tests/tasks/basic-stm-chebyshev.toml']
        main(cl)

    def test_a1(self) -> None:
        compare_frames('basic-stm-chebyshev-a1.csv')

    def test_a1_aux(self) -> None:
        compare_text('basic-stm-chebyshev-a1-aux.txt')

    def test_a2(self) -> None:
        compare_frames('basic-stm-chebyshev-a2.csv')

    def test_a2_aux(self) -> None:
        compare_frames('basic-stm-chebyshev-a2-aux.csv')

    def test_a3(self) -> None:
        compare_frames('basic-stm-chebyshev-a3.csv')

    def test_a3_aux(self) -> None:
        compare_frames('basic-stm-chebyshev-a3-aux.csv')

    def test_a4(self) -> None:
        compare_frames('basic-stm-chebyshev-a4.csv')

    def test_a4_aux(self) -> None:
        compare_frames('basic-stm-chebyshev-a4-aux.csv')

    # a5 tested separately due to grid shape requirements

    def test_a6(self) -> None:
        compare_frames('basic-stm-chebyshev-a6.csv')

    def test_a6_aux(self) -> None:
        compare_frames('basic-stm-chebyshev-a6-aux.csv')

    def test_a7(self) -> None:
        compare_frames('basic-stm-chebyshev-a7.csv')

    def test_a8(self) -> None:
        compare_frames('basic-stm-chebyshev-a8.csv')


class TestBasic8x8NqChebyshev:
    @pytest.fixture(scope='class', autouse=True)
    def _analyze(self) -> None:
        cl = ['seal', 'analyze', '--taskfile', './tests/tasks/basic-8x8-nq-chebyshev.toml']
        main(cl)

    def test_a1(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a1.csv')

    def test_a1_aux(self) -> None:
        compare_text('basic-8x8-nq-chebyshev-a1-aux.txt')

    def test_a2(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a2.csv')

    def test_a2_aux(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a2-aux.csv')

    def test_a3(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a3.csv')

    def test_a3_aux(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a3-aux.csv')

    def test_a4(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a4.csv')

    def test_a4_aux(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a4-aux.csv')

    # a5 tested separately due to grid shape requirements

    def test_a6(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a6.csv')

    def test_a6_aux(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a6-aux.csv')

    def test_a7(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a7.csv')

    def test_a8(self) -> None:
        compare_frames('basic-8x8-nq-chebyshev-a8.csv')
