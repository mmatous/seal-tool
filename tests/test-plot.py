import pytest

from seal.__main__ import main
from tests.util import (
    # OBSERVED_PATH,
    _observed_path,  # noqa: F401
)


class TestBasicRTMChebyshev:
    @pytest.fixture(scope='class', autouse=True)
    def _plot(self) -> None:
        cl = ['seal', 'analyze', '--taskfile', './tests/tasks/basic-rtm-chebyshev.toml']
        main(cl)
        cl = ['seal', 'plot', '--taskfile', './tests/tasks/basic-rtm-chebyshev.toml']
        main(cl)

    # just check that things get plotted for now
    def test_a1(self) -> None:
        pass


class TestPlotA5:
    @pytest.fixture(scope='class', autouse=True)
    def _plot(self) -> None:
        cl = ['seal', 'analyze', '--taskfile', './tests/tasks/a5-rtm-chebyshev.toml']
        main(cl)
        cl = ['seal', 'plot', '--taskfile', './tests/tasks/a5-rtm-chebyshev.toml']
        main(cl)

    # just check that things get plotted for now
    def test_a1(self):
        pass
