import os
import shutil
from collections.abc import Generator
from contextlib import suppress
from pathlib import Path

import pandas as pd
import pytest

EXPECTED_PATH = Path('./tests/expected/')
OBSERVED_PATH = Path('./tests/observed/')


@pytest.fixture(scope='class', autouse=True)
def _observed_path() -> Generator[None, None, None]:
    OBSERVED_PATH.mkdir(parents=True, exist_ok=True)
    yield
    with suppress(FileNotFoundError):
        shutil.rmtree(OBSERVED_PATH)


def compare_frames(observed_file: os.PathLike[str] | str, expected_file: os.PathLike[str] | str | None = None) -> None:
    __tracebackhide__ = True
    observed_file = Path(observed_file)
    observed = pd.read_csv(OBSERVED_PATH / observed_file)
    expected_file = expected_file if expected_file is not None else observed_file
    expected = pd.read_csv(EXPECTED_PATH / expected_file)
    pd.testing.assert_frame_equal(observed, expected, check_like=True, check_index_type=True, check_column_type=True)


def compare_stdout(stdout: str, expected_file: Path | str) -> None:
    __tracebackhide__ = True
    expected = (EXPECTED_PATH / expected_file).read_text(encoding='utf-8')
    assert stdout == expected


def compare_text(observed_file: os.PathLike[str] | str, expected_file: os.PathLike[str] | str | None = None) -> None:
    __tracebackhide__ = True
    observed_file = Path(observed_file)
    observed = (OBSERVED_PATH / observed_file).read_text(encoding='utf-8')
    expected_file = expected_file if expected_file is not None else observed_file
    expected = (EXPECTED_PATH / expected_file).read_text(encoding='utf-8')
    assert observed == expected
