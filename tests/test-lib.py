"""End-to-end tests for seal's library functionality."""

from collections import namedtuple
from dataclasses import asdict
from pathlib import Path

import pandas as pd
import pytest

import seal
from tests.util import EXPECTED_PATH, OBSERVED_PATH, _observed_path, compare_frames, compare_text  # noqa: F401

# analyze


@pytest.fixture
def expected_min_cfg() -> seal.Config:
    return seal.Config(
        quadrat_sides=seal.Sides(x=6.28, y=2.718),
        encounters=Path('/a/b.csv'),
        out_dir=None,
        levels=[1, 2, 3],
        level_strategy='nested-quadrats',
        direction='any',
        include_tiny=True,
        quadrat_types=None,
        distance_type='euclid-diagonal',
        discard_zone_info=False,
        discard_transect_info=False,
        discard_indistinguishable=True,
        exclude_families=[],
        include_families=[],
        exclude_phases=[],
        seed=None,
        use_morph=False,
        analyses=[],
        plot=seal.config.Plot(error_type='pi', error_style='band', logscale_x=True, output_format='svg'),
    )


@pytest.fixture
def stm_config():
    cfg = seal.Config.new(
        './tests/datasets/basic.csv',
        (10, 10),
        distance_type='chebyshev',
        levels=[1, 2, 3],
        level_strategy='striped-transect-merging',
        seed=8001,
        out_dir='./tests/observed',
        quadrat_list=Path('./tests/datasets/qlists/basic.csv'),
    )
    analyses: list[dict[str, str | int]] = [
        {'type': 'a1'},
        {'type': 'a2', 'permutations': 10},
        {'type': 'a3', 'interval': 10},
        {'type': 'a4'},
        {'type': 'a6'},
        {'type': 'a7'},
        {'type': 'a8'},
    ]
    cfg.analyses = seal.make_analyses(analyses)
    return cfg


class TestAnalyze:
    def test_enc_path_df_path(self, stm_config):
        res = seal.analyze(stm_config)

        # just ensure completion and results existence, values are checked over and over in other tests
        assert sorted(res.keys()) == [(f'a{i}', 0) for i in range(1, 9) if i != 5]

    def test_enc_path_qlist_df(self, stm_config):
        qlist = pd.read_csv(stm_config.quadrat_list, index_col=['coord_x', 'coord_y'])
        stm_config.quadrat_list = qlist

        res = seal.analyze(stm_config)

        assert sorted(res.keys()) == [(f'a{i}', 0) for i in range(1, 9) if i != 5]

    def test_enc_path_qlist_none(self, stm_config):
        stm_config.quadrat_list = None

        res = seal.analyze(stm_config)

        assert sorted(res.keys()) == [(f'a{i}', 0) for i in range(1, 9) if i != 5]

    def test_enc_df_qlist_path(self, stm_config):
        enc = pd.read_csv(stm_config.encounters, dtype=seal.util.enc_dtypes()).convert_dtypes()
        stm_config.encounters = enc

        res = seal.analyze(stm_config)

        assert sorted(res.keys()) == [(f'a{i}', 0) for i in range(1, 9) if i != 5]

    def test_enc_df_qlist_df(self, stm_config):
        enc = pd.read_csv(stm_config.encounters, dtype=seal.util.enc_dtypes())
        stm_config.encounters = enc
        qlist = pd.read_csv(stm_config.quadrat_list, index_col=['coord_x', 'coord_y'])
        stm_config.quadrat_list = qlist

        res = seal.analyze(stm_config)

        # just ensure completion and results existence, values are checked over and over in other tests
        assert sorted(res.keys()) == [(f'a{i}', 0) for i in range(1, 9) if i != 5]

    def test_enc_df_qlist_none(self, stm_config):
        enc = pd.read_csv(stm_config.encounters, dtype=seal.util.enc_dtypes()).convert_dtypes()
        stm_config.encounters = enc
        stm_config.quadrat_list = None

        res = seal.analyze(stm_config)

        # just ensure completion and results existence, values are checked over and over in other tests
        assert sorted(res.keys()) == [(f'a{i}', 0) for i in range(1, 9) if i != 5]

    def test_unsuitable_grid_for_levels(self, stm_config):
        enc = pd.read_csv(stm_config.encounters, dtype=seal.util.enc_dtypes()).convert_dtypes()
        qlist = pd.read_csv(stm_config.quadrat_list, index_col=['coord_x', 'coord_y']).convert_dtypes()
        stm_config.encounters = enc
        stm_config.quadrat_list = qlist
        stm_config.levels = [10, 11, 12]

        with pytest.raises(seal.UnsuitableGridError) as einfo:
            seal.analyze(stm_config)

        assert einfo.value.operation == 'striped-transect-merging'
        assert einfo.value.levels == [12]
        assert einfo.value.axis == 'x'
        assert einfo.value.add_info == 'axis dimension must be divisible by maximum level'

    def test_unsuitable_grid_for_a5(self, stm_config):
        stm_config.analyses = seal.make_analyses([{'type': 'a5'}])

        with pytest.raises(seal.UnsuitableGridError) as einfo:
            seal.analyze(stm_config)

        assert einfo.value.operation == 'a5'
        assert einfo.value.levels is None
        assert einfo.value.axis == 'x'
        assert einfo.value.add_info == 'grid must be at least 4 columns wide'
        assert einfo.value.__notes__ == ['is 2 for level 1/3']

    def test_missing_species_col(self, stm_config):
        enc = (
            pd.read_csv(stm_config.encounters, dtype=seal.util.enc_dtypes())
            .convert_dtypes()
            .drop('species', axis='columns')
        )
        stm_config.encounters = enc
        stm_config.quadrat_list = None

        with pytest.raises(AttributeError) as einfo:
            seal.analyze(stm_config)

        assert einfo.value.args[0] == "'DataFrame' object has no attribute 'species'"

    def test_enc_missing_coord_x_col(self, stm_config):
        enc = (
            pd.read_csv(stm_config.encounters, dtype=seal.util.enc_dtypes())
            .convert_dtypes()
            .drop('coord_x', axis='columns')
        )
        stm_config.encounters = enc
        stm_config.quadrat_list = None

        with pytest.raises(KeyError) as einfo:
            seal.analyze(stm_config)

        assert einfo.value.args[0] == "['coord_x'] not in index"

    def test_qlist_missing_coord_x_idx(self, stm_config):
        enc = pd.read_csv(stm_config.encounters, dtype=seal.util.enc_dtypes()).convert_dtypes()
        qlist = pd.read_csv(stm_config.quadrat_list, index_col=['coord_x', 'coord_y']).convert_dtypes()
        qlist.index = qlist.index.rename(('coord_c', 'coord_y'))
        stm_config.encounters = enc
        stm_config.quadrat_list = qlist

        with pytest.raises(KeyError) as einfo:
            seal.analyze(stm_config)

        assert einfo.value.args[0] == 'Level coord_x not found'


class TestCreateConfig:
    def test_minimal_kws(self, expected_min_cfg):
        cfg = seal.Config.new('/a/b.csv', (6.28, 2.718))

        assert cfg == expected_min_cfg

    def test_minimal_dict(self, expected_min_cfg):
        cfg = seal.Config.from_dict({'encounters': '/a/b.csv', 'quadrat_sides': {'x': 6.28, 'y': 2.718}})

        assert cfg == expected_min_cfg

    def test_from_taskfile_accepts_str_path(self):
        cfg = seal.Config.from_taskfile('./tests/tasks/adjust-grid.toml')

        assert cfg is not None


# preprocess


class TestCheckEncounters:
    def test_check_encounters_ignores_extra(self):
        enc = pd.DataFrame({'individuals': [0]})

        res = seal.check_encounters(enc, ['individuals', 'induals'])  # type: ignore[list-item]  # that's the point

        assert res['individuals'] == pd.Index([0])
        assert 'induals' not in res

    def test_check_encounters_raises_for_missing_column_check(self):
        enc = pd.DataFrame({'species': ['A']})  # no name col

        with pytest.raises(KeyError) as einfo:
            seal.check_encounters(enc, ['species-name'])

        assert einfo.value.args[0] == "['name'] not in index"
        assert len(einfo.value.args) == 1


# misc


class TestCheckQuadratList:
    def test_check_quadrat_list_returns_errs(self):
        qlist = pd.DataFrame({'coord_x': [0, 0, 0, -1], 'coord_y': [0, 0, 1, 0]})

        errs = seal.check_quadrat_list(qlist)

        assert errs['dups'] == [1]
        assert errs['min_x'] == -1
        assert errs['n_quadrats'] == 4
        assert len(errs) == 3

    def test_check_quadrat_list_handles_missing_cols(self):
        qlist = pd.DataFrame({'coord_x': []})

        errs = seal.check_quadrat_list(qlist)

        assert errs['missing_cols'] == ['coord_y']
        assert len(errs) == 1


class TestConvertBiolib:
    def test_convert_biolib_returns_conv(self):
        biolib = pd.DataFrame(
            {
                'subsq': ['a'],
                'created': ['2/5/2013'],
                'LATIN': ['A'],
                'QUANTITY': [4],
                'YEAR': [2007],
                'MONTH': [10],
                'DAY': [9],
                'sQuaRe': ['4938'],
            }
        )
        expected = pd.DataFrame(
            {
                'coord_x': [0],
                'coord_y': [0],
                'species': ['A'],
                'date': pd.to_datetime(['2007-10-09']),
                'individuals': [4],
            }
        ).convert_dtypes()

        conv = seal.convert_biolib(biolib)

        pd.testing.assert_frame_equal(conv, expected)

    def test_convert_biolib_raises_for_missing_dates(self):
        biolib = pd.DataFrame({'subsq': ['d']})

        with pytest.raises(AttributeError) as einfo:
            seal.convert_biolib(biolib)

        assert einfo.value.args[0] == "'DataFrame' object has no attribute 'created'"

    def test_convert_biolib_raises_for_invalid_subsq(self):
        biolib = pd.DataFrame(
            {
                'subsq': ['e'],
                'created': ['2/5/2013'],
                'LATIN': ['A'],
                'QUANTITY': [4],
                'YEAR': [2007],
                'MONTH': [10],
                'DAY': [9],
                'sQuaRe': ['4938'],
            }
        )

        with pytest.raises(ValueError) as einfo:  # noqa: PT011
            seal.convert_biolib(biolib)

        assert einfo.value.args[0] == 'subsq'


class TestConvertAopk:
    def test_convert_aopk_returns_conv(self):
        aopk = pd.DataFrame({'DATI_INSERT': ['20071009'], 'dRuH': ['A'], 'POCET': [4], 'sitmap': ['4938']})
        expected = pd.DataFrame(
            {
                'coord_x': [0],
                'coord_y': [0],
                'species': ['A'],
                'date': pd.to_datetime(['2007-10-09']),
                'individuals': [4],
            }
        ).convert_dtypes()

        conv = seal.convert_aopk(aopk)

        pd.testing.assert_frame_equal(conv, expected)

    def test_convert_aopk_raises_for_missing_dates(self):
        aopk = pd.DataFrame({'dRuH': ['A'], 'POCET': [4], 'sitmap': ['4938']})

        with pytest.raises(KeyError) as einfo:
            seal.convert_aopk(aopk)

        assert einfo.value.args[0] == "['dati_insert'] not found in axis"


class TestSarLvlDiff:
    @pytest.fixture(scope='class')
    def a2_aux(self):
        return pd.DataFrame(
            {
                'area': [1, 3, 5, 7, 9, 2, 4, 6, 8, 10],
                'n_species_mean': [1.5, 4.5, 7.5, 10.5, 13.5, 2, 4, 6, 8, 10],
                'level': [1, 1, 1, 1, 1, 2, 2, 2, 2, 2],
            }
        )

    def test_sar_lvl_diff_returns_result(self, a2_aux) -> None:
        expected = pd.DataFrame(
            {
                'diff': [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5],
                'area': list(range(2, 10)),
                'pct_diff': [50.0] * 8,
                'level': [1] * 8,
            }
        )

        diffs = seal.sar_lvl_diff(a2_aux, is_rtm=False)

        pd.testing.assert_frame_equal(diffs.diff, expected)
        assert diffs.ref_lvl == 2

    def test_sar_lvl_diff_raises_with_no_lvl(self, a2_aux):
        a2_aux = a2_aux.drop('level', axis='columns')

        with pytest.raises(KeyError) as einfo:
            seal.sar_lvl_diff(a2_aux)

        assert einfo.value.args[0] == 'level'

    def test_sar_lvl_diff_raises_with_no_area(self, a2_aux):
        a2_aux = a2_aux.drop('area', axis='columns')

        with pytest.raises(AttributeError) as einfo:
            seal.sar_lvl_diff(a2_aux)

        assert einfo.value.args[0] == "'DataFrame' object has no attribute 'area'"


class TestAdjustGrid:
    @pytest.fixture
    def small_grid(self) -> tuple[seal.Config, seal.GridInfo]:
        cfg = seal.Config.new(Path('foo'), (1, 1), levels=range(1, 7))
        enc = pd.DataFrame({'coord_x': [0, 0, 2, 3], 'coord_y': [0, 0, 1, 1], 'species': ['a', 'b', 'b', 'c']})
        qlist = pd.DataFrame(
            index=pd.MultiIndex.from_tuples([(0, 0), (1, 0), (2, 1), (3, 1)], names=['coord_x', 'coord_y'])
        )
        return cfg, seal.GridInfo(enc, qlist)

    @pytest.fixture
    def larger_grid(self) -> tuple[seal.Config, seal.GridInfo]:
        cfg = seal.Config.new(Path('foo'), (1, 1), levels=[1, 2])
        adjust_df = pd.DataFrame(
            {
                'coord_x': [0] * 3 + [1] * 4 + [2] * 3,
                'coord_y': [0, 2, 2, 1, 2, 2, 2, 0, 0, 2],
                'species': ['B', 'A', 'A', 'F', 'C', 'D', 'E', 'H', 'I', 'G'],
                'individuals': [1, 2, 3] * 3 + [4],
            }
        )
        qlist = pd.DataFrame(index=pd.MultiIndex.from_product([range(3), range(3)], names=['coord_x', 'coord_y']))
        return cfg, seal.GridInfo(adjust_df, qlist)

    @pytest.fixture
    def nq_grid(self) -> tuple[seal.Config, seal.GridInfo]:
        cfg = seal.Config.new(Path('foo'), (1, 1), levels=[1, 2])
        # fmt: off
        nq_enc = pd.DataFrame({
            'coord_x': [0] * 9 + [1] * 3 + [2] * 4 + [3] * 6,
            'coord_y': [0, 0, 1, 2, 3, 3, 4, 4, 4, 0, 2, 4, 0, 1, 3, 4, 0, 0, 1, 2, 3, 4],
            'species': [
                'A', 'A', 'B', 'C', 'A', 'A', 'D', 'D', 'D', 'C', 'A',
                'D', 'B', 'B', 'D', 'D', 'B', 'E', 'D', 'E', 'B', 'D',
            ],
            'individuals': [1, 2, 1, 3, 1, 2, 1, 1, 1, 3, 2, 2, 1, 10, 1, 3, 1, 2, 1, 2, 1, 1],
        })
        # fmt: on
        idx = pd.MultiIndex.from_product([range(4), range(5)], names=['coord_x', 'coord_y'])
        qlist = pd.DataFrame(index=idx)
        return cfg, seal.GridInfo(nq_enc, qlist)

    def test_invalid_strategy(self, small_grid):
        cfg_dict = asdict(small_grid[0])
        FakeCfg = namedtuple('FakeConf', cfg_dict.keys())  # type: ignore[misc]  # mock Config without its validation
        cfg_dict['level_strategy'] = 'bogosort'

        with pytest.raises(ValueError) as einfo:  # noqa: PT011
            seal.adjust_grid(FakeCfg(**cfg_dict), small_grid[1])  # type: ignore[arg-type]

        assert 'bogosort' in str(einfo)

    def test_stm_fail_small(self, small_grid):
        small_grid[0].level_strategy = 'striped-transect-merging'

        with pytest.raises(seal.UnsuitableGridError) as einfo:
            seal.adjust_grid(small_grid[0], small_grid[1])

        assert einfo.value.levels == [5, 6]
        assert einfo.value.operation == 'striped-transect-merging'
        assert einfo.value.axis == 'x'
        assert einfo.value.add_info == 'adjustment impossible, axis too small'

    def test_rtm_fail_small(self, small_grid):
        small_grid[0].level_strategy = 'repeated-transect-merging'

        with pytest.raises(seal.UnsuitableGridError) as einfo:
            seal.adjust_grid(small_grid[0], small_grid[1])

        assert einfo.value.levels == [3, 5, 6]
        assert einfo.value.operation == 'repeated-transect-merging'
        assert einfo.value.axis == 'x'
        assert einfo.value.add_info == 'adjustment impossible, axis too small'

    def test_rtm(self, larger_grid):
        larger_grid[0].level_strategy = 'repeated-transect-merging'
        midx = pd.MultiIndex.from_product([range(2), range(3)], names=['coord_x', 'coord_y'])
        exp_qlist = pd.DataFrame(index=midx, columns=[])
        expected_enc = pd.DataFrame(
            {
                'coord_x': [0] * 4 + [1] * 3,
                'coord_y': [1, 2, 2, 2, 0, 0, 2],
                'species': ['F', 'C', 'D', 'E', 'H', 'I', 'G'],
                'individuals': [1, 2, 3] * 2 + [4],
            }
        )
        exp_lost = seal.Lost(3, {'A', 'B'}, 6, {(0, 1), (0, 2), (0, 0)})

        grid, lost = seal.adjust_grid(larger_grid[0], larger_grid[1])

        assert lost == exp_lost
        pd.testing.assert_frame_equal(grid.enc.reset_index(drop=True), expected_enc)
        pd.testing.assert_frame_equal(grid.qlist, exp_qlist)

    def test_stm(self, larger_grid):
        larger_grid[0].level_strategy = 'striped-transect-merging'
        index_tuples = [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2)]
        midx = pd.MultiIndex.from_tuples(index_tuples, names=['coord_x', 'coord_y'])
        exp_qlist = pd.DataFrame(index=midx, columns=[])
        expected_enc = pd.DataFrame(
            {
                'coord_x': [0] * 4 + [1] * 3,
                'coord_y': [1, 2, 2, 2, 0, 0, 2],
                'species': ['F', 'C', 'D', 'E', 'H', 'I', 'G'],
                'individuals': [1, 2, 3] * 2 + [4],
            }
        )

        grid, lost = seal.adjust_grid(larger_grid[0], larger_grid[1])

        assert lost == seal.Lost(3, {'A', 'B'}, 6, {(0, 1), (0, 2), (0, 0)})
        pd.testing.assert_frame_equal(grid.enc.reset_index(drop=True), expected_enc)
        pd.testing.assert_frame_equal(grid.qlist, exp_qlist)

    def test_nq(self, nq_grid):
        nq_grid[0].level_strategy = 'nested-quadrats'
        expected_enc = pd.DataFrame(
            {
                'coord_x': [0] * 6 + [1] * 2 + [2] * 3 + [3] * 5,
                'coord_y': [0, 0, 1, 2, 3, 3, 0, 2, 0, 1, 3, 0, 0, 1, 2, 3],
                'species': ['A', 'A', 'B', 'C', 'A', 'A', 'C', 'A', 'B', 'B', 'D', 'B', 'E', 'D', 'E', 'B'],
                'individuals': [1, 2, 1, 3, 1, 2, 3, 2, 1, 10, 1, 1, 2, 1, 2, 1],
            }
        )
        midx = pd.MultiIndex.from_product([range(4), range(4)], names=['coord_x', 'coord_y'])
        exp_qlist = pd.DataFrame(index=midx, columns=[])

        grid, lost = seal.adjust_grid(nq_grid[0], nq_grid[1])

        assert lost == seal.Lost(6, set(), 9, {(0, 4), (1, 4), (2, 4), (3, 4)})
        pd.testing.assert_frame_equal(grid.enc.reset_index(drop=True), expected_enc)
        pd.testing.assert_frame_equal(grid.qlist, exp_qlist)
