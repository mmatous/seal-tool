import pytest

from seal.__main__ import main
from tests.util import (
    OBSERVED_PATH,
    _observed_path,  # noqa: F401
    compare_frames,
    compare_stdout,
)


def test_preprocess(capsys: pytest.CaptureFixture[str]) -> None:
    cl = [
        'seal',
        'preprocess',
        '--no-drop-nas',
        '--checks',
        'dups',
        'family',
        'individuals',
        'morph-species',
        'nas',
        'quartets',
        'refs',
        'strs',
        'species-name',
        'species-phase-morph',
        '--dataset',
        './tests/datasets/preprocess.csv',
        '--output',
        str(OBSERVED_PATH / 'preprocessed.csv'),
    ]

    main(cl)
    captured = capsys.readouterr().out

    lines = captured.split('\n')
    lines[-2] = lines[-2].replace('\\', '/')  # windows compatibility
    modified_captured = '\n'.join(lines)

    compare_frames('preprocessed.csv')
    compare_stdout(modified_captured, 'preprocess.stdout')
