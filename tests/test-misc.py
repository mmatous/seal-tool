"""Test CLI interface and private functions from misc."""

import pandas as pd
import pytest

from seal.__main__ import main
from seal.misc import Lost, get_lost
from seal.types import GridInfo
from tests.util import (
    OBSERVED_PATH,
    _observed_path,  # noqa: F401
    compare_frames,
)


def test_check_quadrat_list(capsys: pytest.CaptureFixture[str]) -> None:
    cmd = [
        'seal',
        'misc',
        'check-quadrat-list',
        './tests/datasets/qlists/invalid.csv',
    ]

    main(cmd)
    captured = capsys.readouterr().out

    assert captured.splitlines() == [
        "Duplicate coordinates found: Index([4], dtype='int64')",
        'Unusual minimal x coordinate: -1',
        'Unusual minimal y coordinate: 1',
        'Unusual (non-rectangular) number of quadrats, 8 != 9, (3 * 3)',
        "Possibly redundant column(s): ['Unnamed: 0']",
        "Missing or invalid value(s): Index([2], dtype='int64')",
    ]


def test_convert_aopk() -> None:
    cmd = ['seal', 'misc', 'convert-aopk', './tests/datasets/aopk.csv', str(OBSERVED_PATH / 'aopk.csv')]
    main(cmd)

    compare_frames('aopk.csv')


def test_convert_biolib() -> None:
    cmd = ['seal', 'misc', 'convert-biolib', './tests/datasets/biolib.csv', str(OBSERVED_PATH / 'biolib.csv')]
    main(cmd)

    compare_frames('biolib.csv')


def test_get_lost():
    orig_data = pd.DataFrame({'species': ['a', 'a', 'b', 'b', 'b', 'c', 'd']})
    orig_qlist = pd.DataFrame({'coord_x': [0, 0, 1], 'coord_y': [0, 1, 1]})
    adj_data = pd.DataFrame({'species': ['b', 'b', 'b', 'c']})
    adj_qlist = pd.DataFrame({'coord_x': [0], 'coord_y': [0]})
    lost = get_lost(GridInfo(orig_data, orig_qlist), GridInfo(adj_data, adj_qlist))
    assert lost == Lost(3, {'a', 'd'}, None, {(0, 1), (1, 1)})


class TestAdjustGrid:
    def test_adjust_grid_cli(self, capsys):
        cmd = [
            'seal',
            'misc',
            'adjust-grid',
            '--taskfile',
            './tests/tasks/adjust-grid.toml',
            '--output',
            './tests/observed/',
        ]

        main(cmd)
        captured = capsys.readouterr().out

        compare_frames('adjust-grid-rtm.csv')
        compare_frames('adjust-grid-3x3-rtm.csv')
        assert captured.splitlines() == [
            'Old dimension: 3×1. New dimensions: 2×1.',  # noqa: RUF001
            'Lost:',
            '\t3/10 encounters',
            '\t2/9 species: A sp, B sp',
            '\t300.0/900.0 area units',
            '\t1/3 quadrats: (0, 0)',
        ]
