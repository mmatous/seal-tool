# seal

Perform richness-extent-grain analyses inspired by Palmer & White.<sup>[[1]](#paw1)[[2]](#paw2)</sup>

## Setup

### Installation

#### From PyPI via `pip`:

Optionally create a [virtual environment](https://docs.python.org/3/library/venv.html) and activate it.
Then:
```bash
pip install seal-tool
```

Then continue with [Usage](#Usage).

#### Alternatively:

Download `seal` source code from Codeberg [releases](https://codeberg.org/mmatous/seal)
or clone the repository using git.


### First run (non-PyPI installations)

Open the project directory in your
command-line interface.

```
python -m venv ./venv
./venv/Scripts/Activate.ps1
pip install .[dev]
```

You should see `(venv)` before your shell prompt now and `seal --help`
should run the tool without errors.

Continue with [Usage](#Usage).

### Subsequent runs

Done when running the tool any time after the first initial installation.

Linux:
```
source ./venv/bin/activate
seal <subcommand>
```

Windows PowerShell:
```
./venv/Scripts/activate.ps1
seal <subcommand>
```

## Usage

One would generally want to use `preprocess`, `analyse` and `plot` subcommands in this order.
`misc` subcommand contains several scripts that
may be useful while converting or modifying either datasets or quadrat
lists for analysis.

More detailed help can be accessed by `seal --help`
or `seal <subcommand> --help`.

### Preprocess

Preprocess will ensure given dataset is fit to be processed
by the `analyse` command by e.g. sanitizing strings,
warning for missing values or
checking whether species column remains consistent with
name, morph and phase.

Example:
```
seal preprocess --dataset ./datasets/raw.csv --output ./datasets/clean.csv
```

### Strategy

To analyze the influence of grain and  extent on perceived biodiversity, it
is necessary to simulate the existence of quadrats of different size (= grain or level)
with different spacing (extent) in the data.
Seal does so using three different strategies.

Before starting an analysis, it must be specified in the taskfile what strategy should be used.
Striped Transect Merging is the default.

There are currently three strategies.
Following legend applies to all figures in the strategies description.
![Legend](doc/strat-legend.png)

#### Repeated Transect Merging

This strategy merges a number of transects equal to the current level. This
process is done across the entire study grid, resulting in the new grid with
transects of width equal to the width of one quadrat multiplied by the current
level. This will result in a new grid consisting of a number of new transects
being equal to the number of original transects divided by the current level.
If the number of transects in the grid is not divisible by the current level,
the last transect will have a smaller width than the others. This strategy does
not result in any transects being omitted from the analysis. Quadrats not included
in the analysis are depicted as white quadrats in the pictures below. The transects
are indexed left to right, and the merging always starts with the lowest coordinates
(leftmost).

Example:
```
levels = [ 1, 2, 3 ]
level-strategy = "repeated-transect-merging"
```

![Repeated Transect Merging level 1](doc/strat-rtm-lvl1.png)
![Repeated Transect Merging level 2](doc/strat-rtm-lvl2.png)
![Repeated Transect Merging level 3](doc/strat-rtm-lvl3.png)

#### Striped Transect Merging

In this strategy, the entire study grid is divided into sections consisting
of original transects equal to the maximum level requested in the taskfile.
Each section starts with a transect of width equal to the current level, with
the remaining transects being omitted from the analysis. Quadrats not included
in the analysis are depicted as white quadrats in the pictures below. The number
of transects, therefore, remains the same for grids of all levels. In the grid
of maximum level, every transect of the original grid is included. If the number
of transects in the rightmost section is not divisible by the level, the last
transect with the highest coordinates will be narrower than the others.

Example:
```
levels = [ 1, 2, 3 ]
level-strategy = "striped-transect-merging"
```

![Striped Transect Merging level 1](doc/strat-stm-lvl1.png)
![Striped Transect Merging level 2](doc/strat-stm-lvl2.png)
![Striped Transect Merging level 3](doc/strat-stm-lvl3.png)

#### Nested Quadrats
In this strategy, the new quadrats are merged by both transects (axis X)
and zones (axis Y). The grid is divided into square sections with side length
equal to $2^{(\text{max. level} - 1)}$ transects*. For each level, the new square quadrat
is placed on the bottom left corner of each section, with a side length equal
to $2^{(\text{current level} - 1)}$ quadrat(s). The remaining quadrats of the original grid
in the section are omitted from the analysis. Quadrats not included in the analysis
are depicted as white quadrats in the pictures below. In the grid of maximum level,
all quadrats are analysed. Should any quadrat merged this way exceed the grid boundaries,
it will be truncated to remain within the study grids limits. Analogous to Palmer & White.<sup>[[1]](#paw1)[[2]](#paw2)</sup>

\* Meaning width of level 1 = 1,
level 2 = 2, level 3 = 4, level 4 = 8, level 5 = 16, etc.

Example:
```
levels = [ 1, 2, 3 ]
level-strategy = "nested-quadrats"
```

![Nested Quadrats level 1](doc/strat-nq-lvl1.png)
![Nested Quadrats level 2](doc/strat-nq-lvl2.png)
![Nested Quadrats level 3](doc/strat-nq-lvl3.png)

### Analysis

`analyse` subcommand ingests a [taskfile](./tasks/example-task.toml)
to perform analyses requested therein.

Example:
```
seal analyse --taskfile ./tasks/example-task.toml
```

More example taskfiles are available at our [Codeberg repository](./tasks/)
along with some [datasets](./datasets/).

Currently supported analyses are:

#### a1 - Overview

This analysis calculates per-quadrat species number
and number of encountered individuals for each level.

The auxiliary data contain general description of given dataset and various smaller statistics for each level.
Such statistics include, for example, the most common value, mean, median, min and such for each data column.

This helps familiarize oneself with the data and serves as a basic check of the levels-creating strategy.

#### a2 - Species-area relationship

This analysis shows relationship between species richness and area sampled.

Species-area curve is calculated for each level by accumulating quadrats
and tallying the number of species. Since this method is sensitive to order of the
quadrats, number of permutations must be specified and their arithmetic mean is plotted.

The auxiliary data contain statistics of the calculated results.

#### a3 - Distance-dependent species difference

This analysis creates pairwise difference of sets of encountered species
in two quadrats.

Result is the difference as a function of distance of the quadrats.

#### a4 - Richness union

This analysis aims to depict relationship between extent and total richness for various levels (grains). It selects quadrats positioned in a rectangular pattern with various gaps (measured in quadrats) between them, where `max_valid_gap` is limited by the side of study grid with the least amount of quadrats.

Afterwards $|\bigcup_{i=1}^{4} Q_i|$ is computed for every possible pattern, where $Q_{i}$ is a set of species in selected quadrat $i$.

For $max\_valid\_gap = 0$ (one-dimensional grids), the union is limited to two sets, forming endpoints of line segments with various lengths.

#### a5 - Ratio of observed and expected species variance in subgrids

This analysis calculates the ratio of observed and expected richness variance in every valid subgrid.

The expected value is calculated as $$\sum P_i * (1 - P_i)$$  where P<sub>i</sub> is the proportion
of quadrats in the subgrid occupied by species _i_ and the summation is over all the species in the study grid<sup>[[3]](#schluter)</sup>.

Subgrids are created as follows:
1. select $4 \times 4$ quadrats with $n$-sized gap between them, $n$ starts at 0
1. compute ratio described above
1. unless every valid starting point is exhausted, go to step 1
1. increase $n$, go to step 1

#### a6 - Ratio of shared and unique species

This analysis calculates several ratios for pairs of quadrats based on their distance.
1) Ratio of shared species among the quadrats and exclusive species among the quadrats.
2) Ratio of shared species among the quadrats and all species in the study grid of given level.
3) Ratio of shared species among the quadrats and their union.


#### a7 - Jaccard dissimilarity

This analysis calculates Jaccard dissimilarity with regards to species between
all possible pairs of quadrats.

Result is plotted as dissimilarity against the distance
of the quadrats.

Jaccard dissimilarity between quadrats _a_ and _b_ is calculated as:
$$J(a, b) = 1 - \frac{intersect_{ab}}{intersect_{ab} + exclusive_a + exclusive_b}$$



### Plotting

`plot` subcommand ingests [taskfile](./tasks/example-task.toml) to detect results of previous analyses
and present them as graphs.

Generated graphs will be saved to the same directory as
analysis results.

Example:
```
seal plot --taskfile ./tasks/example-task.toml
```

### Misc

`misc` subcommand is a kitchen sink of opinionated
convenience tools.

#### adjust-grid

`adjust-grid` will crop grid edges as necessary to be suitable for chosen strategy and levels.

Adjustments that minimize loss of encounters, species and area will be preferred in this order.

## seal as a library

Install seal onto your system as described [here](./README.md#Installation).

Anything that is exported in the [__init__.py](./src/seal/__init__.py) is considered part of the official API.

### Preprocess

```python
import seal

enc = ...  # obtain dataframe with encounters from somewhere
errors = seal.check_encounters(enc)  # perform default checks for encounters
print(errors['nas'])  # print indices of rows with missing values
print(errors['strs'])  # print dataframe containing likely erroneous strings

checks = ['nas', 'strs', 'individuals']  # specify custom list of checks
errors = seal.check_encounters(enc, checks)
print(errors['individuals'])  # print indices of rows with invalid individuals column
```

### Analysis

```python
import seal
cfg = seal.Config.from_dict({  # setup configuration
	'analyses': [
		{'type': 'a1'},
		{'type': 'a2', 'permutations': 50},
	],
	'encounters': './enc.csv',
	'locality': {
		'sides:': {'x': 10, 'y': 11}, # config up until here is mandatory
		'name': 'Plastic Beach',  # this is optional and used for filtering
	},
	'level_strategy': 'nested-quadrats',
	'levels': [1, 3],  # also optional
	'out-dir': './results',  # path to write results to, must exist beforehand
})
seal.analyze(cfg) # analyze results
# results will be in out-dir

# it is possible to reuse and modify configs
cfg.encounters = my_dataframe  # and to specify encounters as a dataframe directly
cfg.out_dir = None  # set to None to return results in memory
res = seal.analyze(cfg)  # results are returned in res

cfg = seal.Config.from_taskfile('./tasks/mytask.toml')  # config can be read from files
seal.analyze_file(cfg)  # more typing-friendly analysis method, guaranteed to return None
cfg.encounters = my_dataframe
res = seal.analyze_df(cfg)  # guaranteed to return a dict
```

It is also possible to construct `Config` using `.new()` classmethod and `make_analyses()` helper but that may prove less user-friendly.

Configuration options and their default values are documented in the [example taskfile](./tasks/example-task.toml).

### Misc

Subcommands from `seal misc` are also available as library functions.

#### Adjust grid

```python
cfg = seal.Config.from_taskfile('./tasks/mytask.toml')
adj = seal.adjust_grid(cfg)  # perform adjustment for specified levels and strategy
adj_grid = adj[0]  # new grid is available as GridInfo NamedTuple at pos. 0
lost = adj[1]  # information about data lost due to adjustment, Lost NamedTuple
```

#### Check quadrat list

```python
errs = seal.check_quadrat_list(quadrat_list)  # perform checks
if 'dups' in errs:  # check whether any duplicate coordinates were found
	print(errs['dups'])
```

#### Convert AOPK

```python
conv = seal.convert_aopk(aopk_enc)  # perform conversion
print(errs[['coord_x', 'coord_y', 'species']])  # converted dataset contains quadrat coordinates
cfg.encounters = conv
res = seal.analyze_df(cfg)  # and is prepared for analysis
```

#### Convert Biolib

```python
conv = seal.convert_aopk(biolib_enc)  # perform conversion
print(errs[['coord_x', 'coord_y', 'species']])  # converted dataset contains quadrat coordinates
cfg.encounters = conv
res = seal.analyze_df(cfg)  # and is prepared for analysis
```

#### SAR level difference

```python
pd.read_csv('./seal-a2-aux.csv')  # obtain auxiliary data from a2
lvl_diffs, ref_lvl = seal.sar_lvl_diff(a2_aux)
print(f'Differences with reference to level {ref_lvl}:\n{lvl_diffs}')
```

## Development

For unit tests run `python -m doctest ./src/seal/<file>`.

For functional tests run `pytest`.

## References

<a id="paw1">1.</a>
Palmer, M. W., & White, P. S. (1994).
Scale Dependence and the Species-Area Relationship.
The American Naturalist (Vol. 144, Issue 5, pp. 717–740). University of Chicago Press.
https://doi.org/10.1086/285704

<a id="paw2">2.</a>
White, Peter & Palmer, Michael. (1994).
Scale dependence and the species-area relationship.
The University of North Carolina at Chapel Hill University Libraries.
https://doi.org/10.17615/N84A-PD17

<a id="schluter">3.</a>
Schluter, D. (1984).
A variance test for detecting species associations, with some example applications.
Ecology 65, 998–1005.
https://doi.org/10.2307/1938071


### License
The source code—including the tests and documentation—is licensed under [GPLv3](./LICENSE)

`./datasets/data-bmd-sl.csv` is licensed under [CC-BY-SA-4.0](./datasets/LICENSE)

## Acknowledgements

Initial seal protype was funded as part of Influence of sample grain and extent on coral reef fish richness, MUNI/IGA/1076/2021.
